# ['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
#        'PolicyPeriodEnd', 'LossDate', 'ClaimStatus','TotalReserve','TotalPaid',
#        'TotalGrossIncurred','TotalRecoveries','ValuationDate', 'SubmissionID','CarrierName',
#         'NoticeDate','ClosedDate','CarrierPolicyNumber','CoverageType','LossState',
#                'LossDescription','PolicyYear','LocationOfProperty','LossBasis','LossCause','Peril']
# coding: utf-8

# In[3]:
import pandas as pd
import camelot 
import os
# import normalize_heuristic as hr
from dateutil.parser import parse
from tqdm import tqdm
import numpy as np 
import copy
import sys

def is_date(string, fuzzy=False):
    """
    Return whether the string can be interpreted as a date.

    :param string: str, string to check for date
    :param fuzzy: bool, ignore unknown tokens in string if True
    """
    try: 
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False

# 1 Zurich
def Zurich_Post_Processing(pop):
#     print(pop)
    #separating CarrierClaimNumber from claimant
#     pop['CarrierClaimNumber']=pop['Claim #'].apply(lambda x:x.split()[0] if len(x)>3 else '')

#     pop['type']=pop['CarrierClaimNumber'].apply(lambda x:type(x))

#     pop['type']=pop['type'].astype(str)

#     pop_sub=pop[pop['type']=="<class 'str'>"]

#     pop_sub['len']=pop_sub['CarrierClaimNumber'].apply(lambda x:len(x))

#     pop_subs=pop_sub[pop_sub['len']==10]

#     pop_subs['Claimant']=pop_subs['Claim #'].apply(lambda x:x.split()[1] if len(x)>10 else '')

    pop.columns=['CarrierClaimNumber', 'Date of Claimant','LossDate', 
        'LossState', 'ClaimStatus', 'NoticeDate',
        'Indemnity Date', 'ClosedDate', 'BI/Med', 'Exp PD/LT', 'Reserve',
         'Res','TotalPaid', 'TotalGrossIncurred', 'Paid', 'SubmissionID']

    
    pop=pop[pop['CarrierClaimNumber']!='']    
    pop['LossDescription'] = pop['Date of Claimant'].shift(-1)
    pop['CarrierClaimNumber'] = pop['CarrierClaimNumber'].str.extract('(\d+)([^a-zA-Z]+)', expand=True)[0]
    pop['CarrierClaimNumber']=pop['CarrierClaimNumber'].apply(lambda x:x if len(str(x)) == 10  else '')
    pop=pop[pop['CarrierClaimNumber']!=''] 
    return pop



def Zurich1_Post_Processing(pop):
    pop['CarrierClaimNumber'] = pop['Loss Claimant'].str.extract('(\d{10})', expand=True)
    

    pop=pop[pop['Total Paid']!=''] 

    pop.columns=['Coverage Type', 'Loss Claimant', 'LossDate',
       'LossState', 'ClaimStatus', 'NoticeDate', 'ClosedDate',
       'Date', 'Exp BI/Med', 'PD/LT', 'Incurred Reserve', 'Exp Paid', 'TotalPaid',
       'TotalGrossIncurred', 'Total', 'SubmissionID', 'CarrierClaimNumber']
    return pop

def Zurich2_Post_Processing(pop):
    pop['CarrierClaimNumber'] = pop['Loss State'].str.extract('(\d{10})', expand=True)
    

    pop=pop[pop['Total Paid']!=''] 

    pop.columns=['Coverage Type', 'Loss Claimant', 'LossDate',
       'LossState', 'ClaimStatus', 'NoticeDate', 'ClosedDate',
       'Date', 'Exp BI/Med', 'PD/LT', 'Incurred Reserve', 'Exp Paid', 'TotalPaid',
       'TotalGrossIncurred', 'Total', 'SubmissionID', 'CarrierClaimNumber']
    return pop


#2 Cincinatti
def Cincinnati_Post_Processing(popular_table):
    popular_table.columns=[ 'Sorted by Date of Loss  Loc', 'CarrierPolicyNumber', 'PolicyPeriodStart',
       'InsuredName', 'Oc #', 'Cat', 'LossDate', 'LossDescription',
       'CoverageType', 'Claimant/Payee', 'TotalPaid', 'Salv/Subr', 'Expense',
       'End Rsv or Month Closed', 'TotalGrossIncurred',
       'SubmissionID']
    popular_table=popular_table[(popular_table['LossDescription']!='') & (popular_table['CoverageType']!='') ]
    popular_table=popular_table.replace(r'^\s*$', np.nan, regex=True)
    popular_table.fillna(method='ffill',inplace=True)
    return popular_table


#3 CNA
def CNA_Post_Processing(popular_table):
    popular_table.columns=['NoticeDate', 'LossDate', "Occur Date",'ClosedDate', 'CarrierClaimNumber',
       'ClaimStatus', 'LossState', 'Location State', 'im', 'TotalPaid',
       'Total Expenses','Incurred', 'TotalGrossIncurred', 'SubmissionID']
#     popular_table['LossDescription'] = popular_table['ClosedDate'].shift(-1)
    popular_table=popular_table[popular_table['TotalGrossIncurred'] != ""]
    return popular_table

#3 CNA_3
def CNA3_Post_Processing(popular_table):
    popular_table.columns=['NoticeDate', 'LossDate', 'ClosedDate', 'CarrierClaimNumber',
       'ClaimStatus', 'LossState', 'Location State','Indemnity Claimant Name', 'TotalPaid', 'Indemnity  Paid',
       'Total Expenses', 'TotalGrossIncurred', 'Incurred', 'SubmissionID']
    popular_table['LossDescription'] = popular_table['ClosedDate'].shift(-1)
    popular_table=popular_table[popular_table['CarrierClaimNumber'] != ""]
    return popular_table


#4 CNA_2
def CNA2_Post_Processing(popular_table):
    popular_table.columns=['NoticeDate', 'LossDate','Occur Date', 'ClosedDate', 'CarrierClaimNumber',
       'ClaimStatus','Accident Claimant Name', 'LossState', 'Indemnity Claimant Name', 'TotalPaid', 'Indemnity  Paid',
       'Total Expenses', 'TotalGrossIncurred',  'SubmissionID']
    popular_table['LossDescription'] = popular_table['CarrierClaimNumber'].shift(-1)
    popular_table=popular_table[popular_table['TotalGrossIncurred'] != ""]
    return popular_table


#5 AIG
def AIG_Post_Processing(popular_table):
    popular_table['CarrierClaimNumber']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].shift(1)
    popular_table['ClaimStatus']=popular_table['Div / H.O. Status Closed Date'].shift(1)
    popular_table['LossState']=popular_table['Loss State Receipt Date'].shift(1)
    popular_table['LossDescription']=popular_table['Currency:   Loss Description'].shift(2)
   
    popular_table.columns=['LossDate',
       'NoticeDate', 'ClosedDate',
       'Adjuster Name Manager Name', 'Currency:   Loss Description',
       'TotalPaid', 'SubmissionID', 'CarrierClaimNumber',
       'ClaimStatus','LossState','LossDescription']
    
    popular_table=popular_table[popular_table['TotalPaid'] !=""]
    
    return popular_table

#5 AIG1

def AIG1_Post_Processing(popular_table):
    popular_table['CarrierClaimNumber']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].shift(1)
    popular_table['ClaimStatus']=popular_table['Status Closed Date'].shift(1)
    popular_table['LossState']=popular_table['Div / H.O. Loss State Receipt Date'].shift(1)
    popular_table['LossDescription']=popular_table['Currency:   Loss Description'].shift(1)

    popular_table.columns=['LossDate',
       'NoticeDate', 'ClosedDate',
       'Adjuster Name Manager Name', 'Currency:   Loss Description','USD',
       'TotalPaid', 'SubmissionID', 'CarrierClaimNumber',
       'ClaimStatus','LossState','LossDescription']
    
    popular_table=popular_table[popular_table['TotalPaid'] !=""]
    
    return popular_table



# def AIG2_Post_Processing(popular_table):
#     popular_table['CarrierClaimNumber']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].apply(lambda x:  x.split(' /')[0]).shift(1)
    
#     popular_table['ClaimStatus']=popular_table['Status ClosedDate'].shift(1)

#     popular_table.columns=['Claimant Name Claim # / OneClaim # Loss Date',
#        'LossDate', 'Status ClosedDate',
#        'Adjuster Name Manager Name', 'Currency:   Loss Description', 'USD',
#        'TotalGrossIncurred', 'SubmissionID',"CarrierClaimNumber","InsuredName","ClaimStatus"]
    
    

#     new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
#               'TotalGrossIncurred',
#            'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'SubmissionID']
    


#     found_cols=set(popular_table.columns).intersection(new_cols)


#     popular_table[found_cols].index=range(len(popular_table))
#     popular_table=popular_table[popular_table['TotalGrossIncurred'] !=""]
    
#     return popular_table[found_cols]


# #5 Hangover
# def Hanover_Post_Processing(popular_table):
#     #popular_table=popular_table[popular_table['Policy']!='']

#     #renaming columns
#     cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
#            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
#            'TotalGrossIncurred', 'ValuationDate', 'SubmissionID']

#     popular_table.columns=['CPP POLICY #', 'CarrierClaimNumber', 'EFFECTIVE', 'EXPIRATION', 'DATE OF LOSS',
#        'DATE REPORTED', 'OUTSTANDING', 'PAID', 'ALAE', 'RECOVERIES',
#        'TotalGrossIncurred', 'SUBROGATION', 'ClaimStatus', 'SubmissionID']

#     #subsetting columns

#     found=set(popular_table.columns).intersection(set(cols))
#     popular_table=popular_table[found]
#     try:
#         popular_table=popular_table[popular_table['CarrierClaimNumber']!='']
#         popular_table['CarrierClaimNumber']=popular_table['CarrierClaimNumber'].apply(lambda x:x.split()[0] if len(x) == 11  else '')
#     except:
#         pass
    
    
#     return popular_table

##--- Tested --------------###

#1 AIG
# def AIG_1_Post_Processing(popular_table):
#     popular_table['CarrierClaimNumber']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].shift(1)
#     popular_table['InsuredName']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].shift(2)
#     popular_table=popular_table[~popular_table['USD   Loss Paid'].isna()]
    
#     popular_table['is_loss_date']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].apply(lambda x:is_date(x))
#     popular_table=popular_table[popular_table['is_loss_date']==True]
#     popular_table['is_claim_no']=popular_table['CarrierClaimNumber'].apply(lambda x:is_date(x))
#     popular_table['CarrierClaimNumber']=popular_table['CarrierClaimNumber'].apply(lambda x:x.split('/')[0])
    
    
#     popular_table.columns=['LossDate',
#        'LossState Receipt Date', 'Div / H.O. Status ClosedDate',
#        'Adjuster Name Manager Name', 'Currency:   Loss Description',
#        'TotalGrossIncurred', 'Unnamed: 7', 'SubmissionID', 'CarrierClaimNumber',
#        'InsuredName', 'is_loss_date', 'is_claim_no']

#     new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
#               'TotalGrossIncurred',
#            'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'SubmissionID']

#     found_cols=set(popular_table.columns).intersection(new_cols)

#     popular_table[found_cols].index=range(len(popular_table))
    
#     return popular_table[found_cols]

# In[ ]:


#2 Berkley
def Berkley_Post_Processing(popular_table ):
    popular_table=popular_table[popular_table['Claimant Name Open Date Date']!='']
    popular_table.columns=['PolicyPeriodStart', 'PolicyPeriodEnd',
           'ClaimStatus',
           'Claimant Name Open Date Date', 'Claim Closed', 'Claim',
           'CarrierClaimNumber', 'Product Paid', 'TotalGrossIncurred',
           'Total Defense', '', '', 'SubmissionID']

    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
          'TotalGrossIncurred',
       'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'SubmissionID']

    found_cols=set(popular_table.columns).intersection(new_cols)

    popular_table.index=range(len(popular_table))
    return popular_table[found_cols]


# In[ ]: TotalPaid and total reserver , LossDescription  ( not there )
#  'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate'   'TotalGrossIncurred' LineOfBusiness  ValuationDate  ( dilip wanted )


# In[4]:


#5 Firemans Fund
def Firemans_Fund_Post_Processing(popular_table):
   
    popular_table=popular_table[~((popular_table['CarrierClaimNumber Status'] =='') & (popular_table['Loss Amount'] == '') )]
    popular_table['CarrierClaimNumber']=popular_table['CarrierClaimNumber Status'].shift(1)
    try:
        popular_table=popular_table[(popular_table['Description    Coverage'] =='')]
    except:
        popular_table=popular_table[(popular_table['Description Coverage'] =='')]
        
    popular_table['TotalGrossIncurred']=popular_table['Loss Amount'].shift(-1)
    popular_table=popular_table[popular_table['CarrierClaimNumber'] != ""]
#     return popular_table

    
    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID']
    
    popular_table.columns=['ClaimStatus', 'LossDate',
       'Action Cause Atty-Suit Loss Location Claimant',
       'Description    Coverage', 'Paid To Date', 'Outstanding Loss Reserves',
       'Expense To Date', 'Recovery To Date', 'Loss Amount',
       'ClaimStatus: ALL Loss Amount = ALL     Deductible Billed',
       'IAB Expense', 'SubmissionID', 'CarrierClaimNumber', 'TotalGrossIncurred']

    #subsetting columns

    found=set(list(popular_table.columns)).intersection(set(cols))
    popular_table=popular_table[found]

    return popular_table
    
def Firemans_Fund1_Post_Processing(popular_table):
   
    popular_table=popular_table[~((popular_table['CarrierClaimNumber Status'] =='') & (popular_table['Loss Amount'] == '') )]
    popular_table['CarrierClaimNumber']=popular_table['CarrierClaimNumber Status'].shift(1)
    try:
        popular_table=popular_table[(popular_table['Description    Coverage'] =='')]
    except:
        popular_table=popular_table[(popular_table['Description Coverage'] =='')]
        
    popular_table['TotalGrossIncurred']=popular_table['Loss Amount'].shift(-1)
    popular_table=popular_table[popular_table['CarrierClaimNumber'] != ""]
#     return popular_table

    
    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID']
    
    popular_table.columns=['ClaimStatus', 'LossDate',
       'Action Cause Atty-Suit Loss Location Claimant',
       'Description    Coverage', 'Paid To Date', 'Outstanding Loss Reserves',
       'Expense To Date', 'Recovery To Date', 'Loss Amount',
       'ClaimStatus: ALL Loss Amount = ALL     Deductible Billed',
       'IAB Expense', 'SubmissionID', 'CarrierClaimNumber', 'TotalGrossIncurred']

    #subsetting columns

    found=set(list(popular_table.columns)).intersection(set(cols))
    popular_table=popular_table[found]

    return popular_table

# In[5]:


#6 Liberty
def Liberty_Post_Processing(df_clean):
    #df['LossDescription']=df['CarrierClaimNumber LossDescription Code Exposure Group Location LossDescription Accident State'].shift(-1)
    try:
        df_clean['Status']=df_clean['Claimant Name Status Cause  Close Date'].shift(-1)
    except:
        pass
    
    if len(df_clean.columns) ==11 :
        df_clean.columns=[
               'CarrierClaimNumber',
               'InsuredName','LossDate'
               'Loss Date Coverage State    Reopen Date',
               'Carrier Report Date Paid BI/MP BI/MP O/R   Litigation Status',
               'Inc BI/MP Paid PD PD O/R', 'Inc PD Paid Expense Expense O/R',
               'Inc Expense TotalPaid Outstanding Reserve Applied Recovery',
               'TotalGrossIncurred', 'Unnamed: 9', 'SubmissionID', 
               'ClaimStatus']
    else:
        df_clean.columns=['CarrierClaimNumber',
           'InsuredName',
           'LossDate',
           'Carrier Report Date Paid BI/MP BI/MP O/R   Litigation Status',
           'Inc BI/MP Paid PD PD O/R',
           'Inc PD Paid Expense Expense O/R Applied Recovery',
           'Inc Expense TotalPaid Outstanding Reserve', 'TotalGrossIncurred', '',
           'SubmissionID']

    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
              'TotalGrossIncurred',
           'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'SubmissionID']

    df_clean=df_clean[~ (df_clean['TotalGrossIncurred'].isna()) & (df_clean['TotalGrossIncurred'] != '' ) & (df_clean['InsuredName']!='')]
    found_cols=set(df_clean.columns).intersection(new_cols)
    
    return df_clean[found_cols]


def Liberty1_Post_Processing(df_clean):
    #df['LossDescription']=df['CarrierClaimNumber LossDescription Code Exposure Group Location LossDescription Accident State'].shift(-1)
    df_clean['ClaimStatus']=df_clean['Claimant/Driver Name Status   Close Date'].shift(-1)
    
    df_clean.columns=['CarrierClaimNumber',
       'Claimant/Driver Name Status   ClosedDate',
       'Loss Date Jur/Cov/Gar State   Reopen Date',
       'Carrier Report Date  Loss O/R  Litigation Status',
       'Inc Loss Paid Loss Expense O/R',
       'Inc Expense Paid Expense Outstanding Reserve Applied Recovery',
       'TotalGrossIncurred',"", 'SubmissionID','ClaimStatus']
    
    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
              'TotalGrossIncurred',
           'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'SubmissionID']

    df_clean=df_clean[~ (df_clean['TotalGrossIncurred'].isna())  & (df_clean['CarrierClaimNumber']!='') & (df_clean['TotalGrossIncurred'] != '' )]
    found_cols=set(df_clean.columns).intersection(new_cols)
    
    return df_clean[found_cols]

def Liberty2_Post_Processing(df_clean):
    #df['LossDescription']=df['CarrierClaimNumber LossDescription Code Exposure Group Location LossDescription Accident State'].shift(-1)
    df_clean['ClaimStatus']=df_clean['Claimant/Driver Name Status   Close Date'].shift(-1)
    
    df_clean.columns=['CarrierClaimNumber',
       'Claimant/Driver Name Status   ClosedDate',
       'Loss Date Jur/Cov/Gar State   Reopen Date',
       'Carrier Report Date  Loss O/R  Litigation Status',
       'Inc Loss Paid Loss Expense O/R',
       'Inc Expense Paid Expense Outstanding Reserve Applied Recovery',
       'TotalGrossIncurred',"", 'SubmissionID','ClaimStatus']
    
    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
              'TotalGrossIncurred',
           'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'SubmissionID']

    df_clean=df_clean[~ (df_clean['TotalGrossIncurred'].isna())  & (df_clean['CarrierClaimNumber']!='') & (df_clean['TotalGrossIncurred'] != '' )]
    found_cols=set(df_clean.columns).intersection(new_cols)
    
    return df_clean[found_cols]

# In[6]:


#7 Selective
def Selective_Post_Processing(pop):
    #filtering the rows
    pop['payment_there']=pop['Loss  Payments'].apply(lambda x:('$' in x) or (type(x)==float) if type(x)==str else False )
    popular_table=pop[pop['payment_there']]


    #renaming columns
    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID']

    popular_table.columns=['LineOfBusiness', 'Loss  Reserves', 'TotalGrossIncurred',
           'Expense  Payments', 'Loss  Recoveries', 'Loss  Deductibles',
           'Expense  Deductibles', 'SubmissionID', 'payment_there']
   

    found_cols=set(popular_table.columns).intersection(cols)

    popular_table.index=range(len(popular_table))
    return popular_table[found_cols]

def Selective1_Post_Processing(pop):
    #filtering the rows
    pop['payment_there']=pop['Loss  Payments'].apply(lambda x:('$' in x) or (type(x)==float) if type(x)==str else False )
    popular_table=pop[pop['payment_there']]


    #renaming columns
    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID']

    popular_table.columns=['LineOfBusiness', 'Loss  Reserves', 'TotalGrossIncurred',
           'Expense  Payments', 'Loss  Recoveries', 'Loss  Deductibles',
           'Expense  Deductibles','', 'SubmissionID', 'payment_there' ]
   

    found_cols=set(popular_table.columns).intersection(cols)

    popular_table.index=range(len(popular_table))
    return popular_table[found_cols]


def Selective2_Post_Processing(pop):
    #filtering the rows
    pop['payment_there']=pop['Loss  Payments'].apply(lambda x:('$' in x) or (type(x)==float) if type(x)==str else False )
    popular_table=pop[pop['payment_there']]


    #renaming columns
    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID']

    popular_table.columns=['LineOfBusiness', 'Loss  Reserves', 'TotalGrossIncurred',
           'Expense  Payments', 'Loss  Recoveries', 'Loss  Deductibles',
           'Expense  Deductibles','SubmissionID', 'payment_there' ]
   

    found_cols=set(popular_table.columns).intersection(cols)

    popular_table.index=range(len(popular_table))
    return popular_table[found_cols]


# In[ ]:


# # 9 Travellers
def Travelers_Post_Processing(df):
    df['LineOfBusiness'] = df['Claimant'].apply(lambda x:  x.split('Line of Insurance:')[1] if (type(x)==str and 'Line of Insurance:' in x) else None ).shift(1)
    df['Policy Number'] = df['Claimant'].apply(lambda x:  x.split('Policy Year:')[1] if (type(x)==str and 'Policy Year:' in x) else None ).shift(2)
    df['ClaimStatus'] = df['O/C'].apply(lambda x:  x if (x =='C' or x=='O' ) else 'None' )
    df.columns=['Claimant', 'Adj Off', 'FP', 'CarrierClaimNumber', 'LossDate',
       'NoticeDate', 'ClosedDate', 'O/C', 'TotalGrossIncurred', 'Claim', 'Medical',
       'Expense',       'Claimant Adj OfFPCarrierClaimNumber Expense',
       'SubmissionID', 'LineOfBusiness', 'Policy Number','ClaimStatus']

    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID','Policy Number','NoticeDate','ClosedDate']
    
    found=set(df.columns).intersection(set(cols))
    df=df[found]
    df['TotalGrossIncurred'] = df['TotalGrossIncurred'].apply(lambda x:  x if (type(x)==str and '$' in x)else "None" )
    df=df[((df['TotalGrossIncurred']!='None'))]
#     df['CarrierClaimNumber'] = df['CarrierClaimNumber'].apply(lambda x: x).replace('', np.nan)
#     df['CarrierClaimNumber'].fillna(method='ffill',limit = 2, inplace=True)
    df.index=range(len(df))
    return df
# In[ ]:

def Travelers1_Post_Processing(df):
    df['LineOfBusiness'] = df['Claimant Line of Insurance: WC - WORKERS COMP'].apply(lambda x:  x.split('Claimant Line of Insurance:')[1] if (type(x)==str and 'Claimant Line of Insurance:' in x) else None ).shift(2)
#     print(df)
    df['Policy Number'] = df['Claimant Line of Insurance: WC - WORKERS COMP'].apply(lambda x:  x.split('Policy Year:')[1] if (type(x)==str and 'Policy Year:' in x) else None ).shift(2)
    df['ClaimStatus'] = df['O/C'].apply(lambda x:  x if (x =='C' or x=='O' ) else 'None' )
    df.columns=['Claimant', 'Adj Off', 'FP', 'CarrierClaimNumber', 'LossDate',
       'NoticeDate', 'ClosedDate', 'O/C', 'TotalGrossIncurred', 'Claim', 'Medical',
       'Expense',
       'Claimant Adj OfFPCarrierClaimNumber Expense',
       'SubmissionID', 'LineOfBusiness', 'Policy Number','ClaimStatus']

    cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID','Policy Number','NoticeDate','ClosedDate']
    
    found=set(df.columns).intersection(set(cols))
    df=df[found]
    df['TotalGrossIncurred'] = df['TotalGrossIncurred'].apply(lambda x:  x if (type(x)==str and '$' in x)else "None" )
    df=df[((df['TotalGrossIncurred']!='None'))] 
    df.index=range(len(df))
    return df

#10 Hanover ['Auto', 'EFFECTIVE', 'EXPIRATION', 'CLAIMS', 'PAID', 'OUTSTANDING', 'ALAE', 'RECOVERIES', 'INCURRED +ALAE', 'SubmissionID']

def Hanover_Post_Processing(pop):
    pop['LineOfBusiness'] =  pop.columns[0]
    #renaming columns
    cols=['CarrierPolicyNumber','InsuredName', 'CLAIMS', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus','TotalPaid', 'TotalReserve',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID','LineOfBusiness']

    pop.columns=['CarrierPolicyNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'CLAIMS', 'TotalPaid', 'TotalReserve',
       'ALAE', 'RECOVERIES', 'TotalGrossIncurred', 'SubmissionID','LineOfBusiness']
   
    pop = pop[((pop['TotalGrossIncurred']!="") & (pop['CarrierPolicyNumber'] != "" ) & (pop['PolicyPeriodStart'] != "" ))]
    found_cols=set(pop.columns).intersection(cols)
    pop = pop[(pop['CLAIMS'] != '0')]
    
    pop.index=range(len(pop))
    return pop[found_cols]

def Hanover2_Post_Processing(pop):
    pop['LineOfBusiness'] =  pop.columns[0]
    #renaming columns
    cols=['CarrierPolicyNumber','InsuredName', 'CLAIMS', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus','TotalPaid', 'TotalReserve',
           'TotalGrossIncurred', 'ValuationDate', 'SubmissionID','LineOfBusiness']

    pop.columns=['CarrierPolicyNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'CLAIMS', 'TotalPaid', 'TotalReserve',
       'ALAE', 'RECOVERIES', 'TotalGrossIncurred', 'SubmissionID','LineOfBusiness']
           
   
    pop = pop[((pop['TotalGrossIncurred']!="") & (pop['CarrierPolicyNumber'] != "" ) & (pop['PolicyPeriodStart'] != "" ))]
    found_cols=set(pop.columns).intersection(cols)
    pop = pop[(pop['CLAIMS'] != '0')]
    pop.index=range(len(pop))
    return pop[found_cols]

def clean_text(text) : 
    text =str(preprocess_text(text))
    text = text.strip()
#     text = text.lower()
    return text

def clean_text(text) : 
    text =str(preprocess_text(text))
    text = text.strip()
#     text = text.lower()
    return text

def preprocess_text(text):
    '''
    Purpose : Function to convert ascii text to unicode .

    Input : Takes in a text sentence or word .

    Output: Returns the unicode version of the same sentence/word . 
    '''
    text = ''.join(char for char in text if ord(char)<128)
    if sys.version_info[0] == 2:
        return unicode(text)
    else :
        return text

def table_cleaning(table ) :
    table_ = copy.deepcopy(table)
    column_names_list= ['policy #','CarrierClaimNumber', 'PolicyPeriodStart','PolicyPeriodEnd', 'LossDate', 'DATE REPORTED','OUTSTANDING','PAID',' ALAE',' RECOVERIES ','TotalGrossIncurred',' SUBROGATION ','ClaimStatus' , 'SubmissionID']
    table_.reset_index(drop=True, inplace= True)
    column_names_list = [clean_text(x) for x in column_names_list ] 
    print(clean_text(table_.columns[0]).split(" ")[0])
    table_['LineOfBusiness'] = clean_text(table_.columns[0]).split(" ")[0]
    column_names_list.append('LineOfBusiness')
    table_.columns = column_names_list
    table_ = table_.replace('', np.nan , regex=True)
    #finding non-null CarrierClaimNumbers
    claim_no_indexes = []
    original_indexlist = table_.index.tolist()
#     print (original_indexlist)
    for index in original_indexlist: 
        if float(float(table_.loc[index].isna().sum()) / float(table_.shape[1]))  < 0.4 :
            claim_no_indexes.append(index)
#     print ("CarrierClaimNumber indexes : ", claim_no_indexes)
    claimno_ranges = []
    if len(claim_no_indexes) > 1 :
        for i in range(len(claim_no_indexes)) : 
            try :
                claimno_ranges.append(claim_no_indexes[i+1] - claim_no_indexes[i] - 1 )
            except : 
                claimno_ranges.append(int(table_.shape[0] - claim_no_indexes[i]))
    else : 
        claimno_ranges = [table_.shape[0] - claim_no_indexes[0]]
        
    keyvalues_list = column_creater(claim_no_indexes, claimno_ranges, table_)
    drop_indexes = [x for x in table_.index.tolist() if x not in claim_no_indexes]
    table_1 = table_.drop(index= drop_indexes , axis = 0)
    table_1 = table_1.reset_index()
    table_1 = table_1.drop(columns=['index'] , axis =1 )
    try : 
        table_1['claimant names'] = pd.Series(keyvalues_list[0])
        table_1['loss explanations'] = pd.Series(keyvalues_list[1])
        table_1['loss description'] = pd.Series(keyvalues_list[2])
        table_1['report run date'] = keyvalues_list[3]
        table_1['ValuationDate'] = keyvalues_list[4]
    except : 
        pass
    # table_1['filename'] = filename
    
    
    return table_1

def column_creater(claim_no_indexes, claimno_ranges, table_):
    Claimant_names = [] ; Loss_explanations = [] ; Descs = [] ; Report_run_date = '' ; Loss_Evaluation_Date = '' 
    for i in range(len(claim_no_indexes)) : 
        search_rows = []
        for j in range(claimno_ranges[i]) : 
            try : 
                if claim_no_indexes[i] + j +1 <= table_.shape[0] :
                    search_rows = search_rows + table_.loc[claim_no_indexes[i] + j +1 ].dropna().tolist()
            except  :
                pass
        search_rows = [clean_text(x) for x in search_rows]
        keyvalues_list = keyword_search(search_rows)
        Claimant_names.append(keyvalues_list[0])
        Loss_explanations.append(keyvalues_list[1])
        Descs.append(keyvalues_list[2])
        if len(keyvalues_list) == 5 : 
            Report_run_date = keyvalues_list[3]
            Loss_Evaluation_Date = keyvalues_list[4]
    try : 
        return [Claimant_names ,Loss_explanations, Descs , Report_run_date, Loss_Evaluation_Date]
    except :
        return [Claimant_names, Loss_explanations, Descs]

def keyword_search(list_) :
    Claimant_name = "" ; Loss_explanation = "" ; Desc = ""
    for i in range(len(list_)):
        try :
            if "loss explanation:" in list_[i] : 
                Loss_explanation = list_[i].split("loss explanation:")[1]
            elif "claimant:" in list_[i] : 
                Claimant_name = list_[i].split("claimant:")[1]
            elif "desc:" in list_[i] : 
                Desc = list_[i].split("desc:")[1] 
            elif "report run date:" in list_[i] : 
                Report_run_date = list_[i].split("report run date:")[1]
            elif "loss evaluation date:" in list_[i] :
                Loss_Evaluation_Date = list_[i].split("loss evaluation date:")[1] 
        except IndexError : 
            print(list_)
            print ([Claimant_name , Loss_explanation, Desc ])
    try : 
        return [Claimant_name , Loss_explanation, Desc , Report_run_date , Loss_Evaluation_Date ]
    except : 
        return [Claimant_name , Loss_explanation, Desc ]


def Hanover1_Post_Processing(dataframe_) : 
    Master_table = pd.DataFrame(columns= dataframe_.columns)
    if dataframe_.shape[0] != 0 : 
        Master_table = table_cleaning(dataframe_)
        
    Master_table.rename(columns = {'Claim #':'CarrierClaimNumber'}, inplace = True) 
    return Master_table

def Hanover4_Post_Processing(dataframe_) : 
    Master_table = pd.DataFrame(columns= dataframe_.columns)
    if dataframe_.shape[0] != 0 : 
        Master_table = table_cleaning(dataframe_)
        
    Master_table.rename(columns = {'Claim #':'CarrierClaimNumber'}, inplace = True) 
    return Master_table

def table_cleaning1(table ) :
    table_ = copy.deepcopy(table)
    column_names_list= ['policy #','CarrierClaimNumber', 'PolicyPeriodStart','PolicyPeriodEnd', 'LossDate', 'DATE REPORTED','OUTSTANDING','','PAID',' ALAE',' RECOVERIES ','TotalGrossIncurred',' SUBROGATION ','ClaimStatus' , 'SubmissionID']
    table_.reset_index(drop=True, inplace= True)
    column_names_list = [clean_text(x) for x in column_names_list ] 
    table_['LineOfBusiness'] = clean_text(table_.columns[0]).split(" ")[0]
    column_names_list.append('LineOfBusiness')
    table_.columns = column_names_list
    table_ = table_.replace('', np.nan , regex=True)
    #finding non-null CarrierClaimNumbers
    claim_no_indexes = []
    original_indexlist = table_.index.tolist()
#     print (original_indexlist)
    for index in original_indexlist: 
        if float(float(table_.loc[index].isna().sum()) / float(table_.shape[1]))  < 0.4 :
            claim_no_indexes.append(index)
#     print ("CarrierClaimNumber indexes : ", claim_no_indexes)
    claimno_ranges = []
    if len(claim_no_indexes) > 1 :
        for i in range(len(claim_no_indexes)) : 
            try :
                claimno_ranges.append(claim_no_indexes[i+1] - claim_no_indexes[i] - 1 )
            except : 
                claimno_ranges.append(int(table_.shape[0] - claim_no_indexes[i]))
    else : 
        claimno_ranges = [table_.shape[0] - claim_no_indexes[0]]
        
    keyvalues_list = column_creater(claim_no_indexes, claimno_ranges, table_)
    drop_indexes = [x for x in table_.index.tolist() if x not in claim_no_indexes]
    table_1 = table_.drop(index= drop_indexes , axis = 0)
    table_1 = table_1.reset_index()
    table_1 = table_1.drop(columns=['index'] , axis =1 )
    try : 
        table_1['claimant names'] = pd.Series(keyvalues_list[0])
        table_1['loss explanations'] = pd.Series(keyvalues_list[1])
        table_1['loss description'] = pd.Series(keyvalues_list[2])
        table_1['report run date'] = keyvalues_list[3]
        table_1['ValuationDate'] = keyvalues_list[4]
    except : 
        pass
    # table_1['filename'] = filename
    
    
    return table_1

def Hanover3_Post_Processing(dataframe_) : 
    Master_table = pd.DataFrame(columns= dataframe_.columns)
    if dataframe_.shape[0] != 0 : 
        Master_table = table_cleaning1(dataframe_)
        
    Master_table.rename(columns = {'Claim #':'CarrierClaimNumber'}, inplace = True) 
    return Master_table



def Amtrust_Post_Processing(popular_table) : 
    """
    Input : Table dataframe belonging to the Amtrust Carrier Schema 1 
    """
    # mapping the column names in the file to the column names of the master dataframe file 
    mapping_dict = {
        'Claim No':'CarrierClaimNumber',
        'Policy Number':'Policy Number',
        'Pol. Eff Date':'PolicyPeriodStart',
        'Insured':'InsuredName',
        'DOL':'LossDate',
        'Status':'ClaimStatus',
        'Category':'LineOfBusiness',
        'Loss Description':'Loss Description',
        'Total1':'TotalPaid',
        'Total2':'Total Reserve',
        'Total3':'TotalGrossIncurred',
        'Total4':'Total Recoveries',
        'Juris St':'LossState',
        'Date Rcvd':'Date Reported',
        'Cause':'Loss Cause',
        'Submission ID': 'Submission ID'
        }

    master_df = pd.DataFrame(columns=mapping_dict.values())
    #tracker to find out the unique row items since the a single claim spans across mulitple rows 
    tracker = list(map(lambda x : 1 if x=='1' else 0,popular_table[' Claim No Converted # Policy Number Pol. Eff Date'].values))
    tracker_ind = [ind for ind,val in enumerate(tracker) if val==1]
    tracker_ind.append(len(popular_table))
    data = [tracker_ind[n:n+2] for n in range(len(tracker_ind))]
    final_data = []
    counter = 0
    for x in data[:-1]:
        final_data.extend([counter]*(x[1]-x[0])) 
        counter+=1
    
    popular_table.insert(0,'tracker',final_data)
    popular_table.columns = [x if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
    popular_table = popular_table.groupby(by=['tracker'],as_index=False).agg('||'.join)
    # merging multiple row items column-wise by \\ to identify which item corresponds to which column 
    first_list = popular_table[' Claim No Converted # Policy Number Pol. Eff Date'].map(lambda x:x.split('||')).values
    first_list = [[x for x in y if x] for y in first_list]
    second_list = popular_table[' Claimant Class Cd Juris St Insured'].map(lambda x:x.split('||')).values
    second_list = [[x for x in y if x] for y in second_list]
    third_list = popular_table[' Department DOL First Aware Date Rcvd'].map(lambda x:x.split('||')).values
    third_list = [[x for x in y if x] for y in third_list]
    fourth_list = popular_table['  Status Category Adjuster'].map(lambda x:x.split('||')).values
    fourth_list = [[x for x in y if x] for y in fourth_list]
    fifth_list = popular_table[' Loss Location Part Injured Cause Loss Description'].map(lambda x:x.split('||')).values
    fifth_list =[[x for x in y if x] for y in fifth_list]
    sixth_list = popular_table['    Total'].map(lambda x:x.split('||')).values
    sixth_list = [[x for x in y if x] for y in sixth_list]
    seventh_list = popular_table['Submission ID'].map(lambda x:x.split('||')).values
    seventh_list = [[x for x in y if x] for y in seventh_list]
    eight_list = popular_table['NA_7'].map(lambda x:x.split('||')).values
    eight_list = [[x for x in y if x] for y in eight_list]
    nine_list = popular_table['NA_12'].map(lambda x:x.split('||')).values
    nine_list = [[x for x in y if x] for y in nine_list]
    
    def lob(fourth_list, fifth_list) : 
        """
        Function to extract LineOfBusiness. 
        """
        lob_ = []
        for i in range(len(fourth_list)) : 
            if len(fourth_list[i][0].split()) != 1: 
                if fifth_list[i][1].isupper() : 
                    lob_.append(fourth_list[i][0].split()[-1] + fifth_list[i][1])
                else : 
                    lob_.append(fourth_list[i][0].split()[-1] + fifth_list[i][2])
            else : 
                lob_.append(fifth_list[i][1])
        return lob_
    # dictionary with column names as keys mappend to their necessary function 
    dispatcher = {
        'CarrierClaimNumber': lambda : [x[1] for x in first_list],
        'Policy Number': lambda : [x[-3][-1]+x[-2] for  x in first_list],
        'PolicyPeriodStart' : lambda : [x[-1] for x in first_list],
        'LossState' : lambda :  [x[2] for x in second_list],
        'InsuredName': lambda : [x[3] for x in second_list],
        'LossDate' : [x[0] for x in third_list],
        'Date Reported' : lambda : [x[2] for x in third_list],
        'ClaimStatus' : lambda :[x[0] for x in fourth_list],
        'Loss Cause' : lambda :[x[-2] for x in eight_list],
        'Loss Description' : lambda :[' '.join(x[2:]) for x in fifth_list],
        'TotalGrossIncurred': lambda :[sixth_list[i][v] for i, v in enumerate([-1 if x[-1] == "Incurred" else x.index("Incurred") for x in eight_list ])],
        'Total Reserve' : lambda : [sixth_list[i][0] for i in range(len(sixth_list))],
        'Total Recoveries': lambda :[nine_list[i][-1] for i in range(len(nine_list))],
        'TotalPaid' : lambda : [nine_list[i][0] for i in range(len(nine_list))],
        'LineOfBusiness': lambda : lob(fourth_list, fifth_list),
        'Submission ID': lambda : [x[0] for x in seventh_list],
        'fallback':lambda df : ['NA']*len(new_df)
        }
    # iterating over each column name and appending the row items 
    new_df = pd.DataFrame(columns=mapping_dict.values())
    for col in master_df.columns :
        try:
            new_df[col] = dispatcher[col]()
        except:
            new_df[col] = dispatcher['fallback'](new_df)
    master_df = pd.concat([master_df, new_df])
    master_df.index = range(len(master_df))
    master_df['ClaimStatus'] = master_df['ClaimStatus'].map(lambda x:x.split()[0])
    master_df['Submission ID'] = master_df['Submission ID'].map(lambda x:x.split('/')[-1].strip('.pdf'))
    return master_df

def Amtrust1_Post_Processing(popular_table) : 
    """
    Input : Table dataframe belonging to the Amtrust Carrier Schema 2 & 4 
    """
    # mapping the column names in the file to the column names of the master dataframe file 
    mapping_dict = {
        'Claim No':'CarrierClaimNumber',
        'Policy Number':'Policy Number',
        'Pol. Eff Date':'PolicyPeriodStart',
        'Insured':'InsuredName',
        'DOL':'LossDate',
        'Status':'ClaimStatus',
        'Category':'LineOfBusiness',
        'Loss Description':'Loss Description',
        'Total1':'TotalPaid',
        'Total2':'Total Reserve',
        'Total3':'TotalGrossIncurred',
        'Total4':'Total Recoveries',
        'Juris St':'LossState',
        'Date Rcvd':'Date Reported',
        'Cause':'Loss Cause',
        'Submission ID': 'Submission ID'
        }
    master_df = pd.DataFrame(columns=mapping_dict.values())
    def total_fields(term):
        """
        Function to extract all the total fields. 
        """
        fifth_list = []
        for i in range(popular_table.shape[0]) : 
            mets = np.array(list(map(lambda x:x.split('||'), popular_table.iloc[i].values[1:-1])))
            indice = np.argwhere(mets == term)[0][1]
            fifth_list.append(list(filter(lambda x : x.strip().replace(",","").isdigit(), mets[:,indice])))
        return list(map(lambda x: x[-1], fifth_list))

    def parse_(string, fuzzy = False) : 
        """
        Function to check if a string is a date.
        """
        try: 
            parse(string, fuzzy=fuzzy)
            return True
        except ValueError:
            return False

    def loss_desc(fourth_list) : 
        """
        Function to merge all the description strings.
        """
        dates = [x[-1] for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]]
        return [" ".join(fourth_list[i][fourth_list[i].index(dates[i])+1:]) for i in range(len(fourth_list))]

    
    popular_table.columns = [x if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
    
    #tracker to find out the unique row items since the a single claim spans across mulitple rows 
    tracker = list(map(lambda x : 1 if x=='1' else 0,popular_table['NA_1'].values))
    tracker_ind = [ind for ind,val in enumerate(tracker) if val==1]
    tracker_ind.append(len(popular_table))
    data = [tracker_ind[n:n+2] for n in range(len(tracker_ind))]
    final_data = []
    counter = 0
    for x in data[:-1]:
        final_data.extend([counter]*(x[1]-x[0])) 
        counter+=1
    popular_table.insert(0,'tracker',final_data)
    popular_table.columns = [x.strip() if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
    popular_table = popular_table.groupby(by=['tracker'],as_index=False).agg('||'.join)
    
    # merging multiple row items column-wise by \\ to identify which item corresponds to which column 
    first_list = popular_table['Claim No Converted # Policy Number Pol. Eff Date'].map(lambda x:x.split('||')).values
    first_list = [[x for x in y] for y in first_list]
    second_list = popular_table['Claimant Class Cd Juris St Insured'].map(lambda x:x.split('||')).values
    second_list= [[x for x in y if x] for y in second_list]
    third_list = popular_table['Department DOL First Aware Date Rcvd'].map(lambda x:x.split('||')).values
    third_list = [[x for x in y if x] for y in third_list]
    fourth_list = popular_table['Loss Location Nature Employee Lag Reporting Lag'].map(lambda x:x.split('||')).values
    fourth_list = [[x for x in y if x] for y in fourth_list]
    seventh_list = popular_table['Submission ID'].map(lambda x:x.split('||')).values
    seventh_list = [[x for x in y if x] for y in seventh_list]
    # dictionary with column names as keys mappend to their necessary function 
    dispatcher = {
        'CarrierClaimNumber': lambda : [x[1] for x in first_list],
        'Policy Number': lambda : [x[2][-1]+x[3] if len(x[2].split()) > 1 else x[3] for x in first_list],
        'PolicyPeriodStart' : lambda : [x[4] for x in first_list],
        'LossState' : lambda :  [x[0] for x in second_list],
        'InsuredName': lambda : [x[3] for x in second_list],
        'LossDate' : [x[0] if len(x)>0 else "NA" for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]],
        'Date Reported' : lambda : [x[1] if len(x) > 1 else "NA" for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]],
        'ClaimStatus' : lambda :[x[1].split()[0] for x in third_list],
        'LineOfBusiness' : lambda :[x[1].split()[1]+ x[2] if len(x[1].split()) > 1 else x[2] for x in third_list],
        'Loss Description' : lambda :loss_desc(fourth_list) , 
        'TotalGrossIncurred': lambda :total_fields("Incurred"),
        'Total Reserve' : lambda :total_fields("Reserves") ,
        'Total Recoveries': lambda :total_fields("Recoveries") ,
        'TotalPaid' : lambda :total_fields("Payments") ,
        'Submission ID': lambda : [x[0] for x in seventh_list],
        'fallback':lambda df : ['NA']*len(new_df)
        }
    # iterating over each column name and appending the row items 
    new_df = pd.DataFrame(columns=mapping_dict.values())
    for col in master_df.columns :
        try:
            new_df[col] = dispatcher[col]()
        except:
            new_df[col] = dispatcher['fallback'](new_df)
    master_df = pd.concat([master_df, new_df])
    master_df.index = range(len(master_df))
    master_df['ClaimStatus'] = master_df['ClaimStatus'].map(lambda x:x.split()[0])
    master_df['Submission ID'] = master_df['Submission ID'].map(lambda x:x.split('/')[-1].strip('.pdf'))
    return master_df


def Amtrust2_Post_Processing(popular_table) : 
    """
    Input : Table dataframe belonging to the Amtrust Carrier Schema 2 & 4 
    """
    # mapping the column names in the file to the column names of the master dataframe file 
    mapping_dict = {
        'Claim No':'CarrierClaimNumber',
        'Policy Number':'Policy Number',
        'Pol. Eff Date':'PolicyPeriodStart',
        'Insured':'InsuredName',
        'DOL':'LossDate',
        'Status':'ClaimStatus',
        'Category':'LineOfBusiness',
        'Loss Description':'Loss Description',
        'Total1':'TotalPaid',
        'Total2':'Total Reserve',
        'Total3':'TotalGrossIncurred',
        'Total4':'Total Recoveries',
        'Juris St':'LossState',
        'Date Rcvd':'Date Reported',
        'Cause':'Loss Cause',
        'Submission ID': 'Submission ID'
        }
    master_df = pd.DataFrame(columns=mapping_dict.values())
    def total_fields(term):
        """
        Function to extract all the total fields. 
        """
        fifth_list = []
        for i in range(popular_table.shape[0]) : 
            mets = np.array(list(map(lambda x:x.split('||'), popular_table.iloc[i].values[1:-1])))
            indice = np.argwhere(mets == term)[0][1]
            fifth_list.append(list(filter(lambda x : x.strip().replace(",","").isdigit(), mets[:,indice])))
        return list(map(lambda x: x[-1], fifth_list))

    def parse_(string, fuzzy = False) : 
        """
        Function to check if a string is a date.
        """
        try: 
            parse(string, fuzzy=fuzzy)
            return True
        except ValueError:
            return False

    def loss_desc(fourth_list) : 
        """
        Function to merge all the description strings.
        """
        dates = [x[-1] for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]]
        return [" ".join(fourth_list[i][fourth_list[i].index(dates[i])+1:]) for i in range(len(fourth_list))]

    
    popular_table.columns = [x if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
    
    #tracker to find out the unique row items since the a single claim spans across mulitple rows 
    tracker = list(map(lambda x : 1 if x=='1' else 0,popular_table['NA_1'].values))
    tracker_ind = [ind for ind,val in enumerate(tracker) if val==1]
    tracker_ind.append(len(popular_table))
    data = [tracker_ind[n:n+2] for n in range(len(tracker_ind))]
    final_data = []
    counter = 0
    for x in data[:-1]:
        final_data.extend([counter]*(x[1]-x[0])) 
        counter+=1
    popular_table.insert(0,'tracker',final_data)
    popular_table.columns = [x.strip() if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
    popular_table = popular_table.groupby(by=['tracker'],as_index=False).agg('||'.join)
    
    # merging multiple row items column-wise by \\ to identify which item corresponds to which column 
    first_list = popular_table['Claim No Converted # Policy Number Pol. Eff Date'].map(lambda x:x.split('||')).values
    first_list = [[x for x in y] for y in first_list]
    second_list = popular_table['Claimant Class Cd Juris St Insured'].map(lambda x:x.split('||')).values
    second_list= [[x for x in y if x] for y in second_list]
    third_list = popular_table['Department DOL First Aware Date Rcvd'].map(lambda x:x.split('||')).values
    third_list = [[x for x in y if x] for y in third_list]
    fourth_list = popular_table['Loss Location Nature Employee Lag Reporting Lag'].map(lambda x:x.split('||')).values
    fourth_list = [[x for x in y if x] for y in fourth_list]
    seventh_list = popular_table['Submission ID'].map(lambda x:x.split('||')).values
    seventh_list = [[x for x in y if x] for y in seventh_list]
    # dictionary with column names as keys mappend to their necessary function 
    dispatcher = {
        'CarrierClaimNumber': lambda : [x[1] for x in first_list],
        'Policy Number': lambda : [x[2][-1]+x[3] if len(x[2].split()) > 1 else x[3] for x in first_list],
        'PolicyPeriodStart' : lambda : [x[4] for x in first_list],
        'LossState' : lambda :  [x[0] for x in second_list],
        'InsuredName': lambda : [x[3] for x in second_list],
        'LossDate' : [x[0] if len(x)>0 else "NA" for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]],
        'Date Reported' : lambda : [x[1] if len(x) > 1 else "NA" for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]],
        'ClaimStatus' : lambda :[x[1].split()[0] for x in third_list],
        'LineOfBusiness' : lambda :[x[1].split()[1]+ x[2] if len(x[1].split()) > 1 else x[2] for x in third_list],
        'Loss Description' : lambda :loss_desc(fourth_list) , 
        'TotalGrossIncurred': lambda :total_fields("Incurred"),
        'Total Reserve' : lambda :total_fields("Reserves") ,
        'Total Recoveries': lambda :total_fields("Recoveries") ,
        'TotalPaid' : lambda :total_fields("Payments") ,
        'Submission ID': lambda : [x[0] for x in seventh_list],
        'fallback':lambda df : ['NA']*len(new_df)
        }
    # iterating over each column name and appending the row items 
    new_df = pd.DataFrame(columns=mapping_dict.values())
    for col in master_df.columns :
        try:
            new_df[col] = dispatcher[col]()
        except:
            new_df[col] = dispatcher['fallback'](new_df)
    master_df = pd.concat([master_df, new_df])
    master_df.index = range(len(master_df))
    master_df['ClaimStatus'] = master_df['ClaimStatus'].map(lambda x:x.split()[0])
    master_df['Submission ID'] = master_df['Submission ID'].map(lambda x:x.split('/')[-1].strip('.pdf'))
    return master_df


# one beacon ----------------------
def onebeacon_Post_Processing(pop):
    pop.columns = ['ClaimStatus', 'Loss Claimant', 'Loss Age Paid $',
       'CarrierClaimNumber', 'Lgl Exp Pd   Pd $', 'TotalPaid',
       'Subro/  Rsv $ Rcvd$', 'Ded Amt', '', '', '', 'TotalGrossIncurred', 'SubmissionID']
    pop['CoverageType'] = pop['ClaimStatus'].shift(-1)
#     pop ['CarrierClaimNumber'] = pop['Alloc Exp  Rsv $ $'].apply(lambda x:x.split(" ")[0])
    pop=pop[pop['TotalGrossIncurred']!=''] 

    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
              'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
              'ValuationDate', 'SubmissionID']

    found=list(set(pop.columns).intersection(set(new_cols)))
    phase1=pop[found]
    return phase1

def onebeacon1_Post_Processing(pop):
    pop.columns = ['ClaimStatus', 'CarrierClaimNumber', 'Alloc Exp  Age Paid $',
       'Loss  Rsv $ $', 'Lgl Exp Pd Rsv $ Pd $', 'TotalPaid', 'Subro/',
       'Total $ Rcvd$', 'Ded Amt', '', 'TotalGrossIncurred', '', 'SubmissionID']
    pop['CoverageType'] = pop['CarrierClaimNumber'].shift(-1)
#     pop ['CarrierClaimNumber'] = pop['Alloc Exp  Rsv $ $'].apply(lambda x:x.split(" ")[0])
    pop=pop[pop['TotalGrossIncurred']!=''] 

    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
              'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
              'ValuationDate', 'SubmissionID']

    found=list(set(pop.columns).intersection(set(new_cols)))
    phase1=pop[found]
    return phase1

def nationwide_Post_Processing(pop):
    pop.columns = ['Type Premises Claim # ID Loss', 'Date of Location Loss',
       'LossDate', 'NoticeDate',
       'TotalPaid', 'Closed of Subro', 'TotalGrossIncurred', '','Loss Cause','CoverageType', '',
       'SubmissionID']
    pop['CarrierClaimNumber'] = pop['Type Premises Claim # ID Loss'].shift(1)
    pop['ClaimStatus'] = pop['Type Premises Claim # ID Loss'].shift(-1)
    pop['LossDescription'] = pop['Type Premises Claim # ID Loss'].shift(-2)
    pop['LossState'] = pop['Date of Location Loss'].shift(-1)

    pop=pop[pop['TotalGrossIncurred'].apply(lambda x:('$' in x) if type(x)==str else False)] 
    pop=pop[pop['CarrierClaimNumber'].apply(lambda x:('$' not in x) if len(x)>18 else False)]
    
    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
              'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
              'ValuationDate', 'SubmissionID','LossState','CoverageType','Loss Cause']

    found=list(set(pop.columns).intersection(set(new_cols)))
    phase1=pop[found]
    return phase1

def nationwide1_Post_Processing(pop):
  
    pop.columns = ['Type Premises Claim # ID Loss', 'Date of Location Loss',
       'LossDate', 'NoticeDate',
       'TotalPaid', 'Closed of Subro', 'TotalGrossIncurred','Loss Cause','CoverageType', '', '',
       'SubmissionID']
    pop['CarrierClaimNumber'] = pop['Type Premises Claim # ID Loss'].shift(1)
    pop['ClaimStatus'] = pop['Type Premises Claim # ID Loss'].shift(-1)
    pop['LossDescription'] = pop['Type Premises Claim # ID Loss'].shift(-2)
    pop['LossState'] = pop['Date of Location Loss'].shift(-1)

    pop=pop[pop['TotalGrossIncurred'].apply(lambda x:('$' in x) if type(x)==str else False)] 
    pop=pop[pop['CarrierClaimNumber'].apply(lambda x:('$' not in x) if len(x)>18 else False)]
    
    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
              'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
              'ValuationDate', 'SubmissionID','LossState','Loss Cause','CoverageType']

    found=list(set(pop.columns).intersection(set(new_cols)))
    phase1=pop[found]
    return phase1

def nationwide2_Post_Processing(pop):
    pop.columns = ['Type Premises Claim # ID Loss', 'LossDate',
       'NoticeDate', 'TotalPaid', 'Closed Incurred Subro',
       'TotalGrossIncurred', 'CoverageType', '', '', 'SubmissionID']
    pop['CarrierClaimNumber'] = pop['Type Premises Claim # ID Loss'].shift(2)
    pop['ClaimStatus'] = pop['LossDate'].shift(-2)
    pop['LossState'] = pop['Type Premises Claim # ID Loss'].shift(-3)
    pop['LossDescription'] = pop['Type Premises Claim # ID Loss'].shift(-4)
    pop['Loss Cause'] = pop['Type Premises Claim # ID Loss'].shift(1)

    pop=pop[pop['TotalGrossIncurred'].apply(lambda x:('$' in x) if type(x)==str else False)] 
    pop=pop[pop['CarrierClaimNumber'].apply(lambda x:('$' not in x) if len(x)>18 else False)]

    new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
              'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
              'ValuationDate', 'SubmissionID','LossState','NoticeDate']

    found=list(set(pop.columns).intersection(set(new_cols)))
    phase1=pop[found]
    return phase1

# Axis 
def Axis_Post_Processing(popular_table):

    popular_table.columns=['CarrierClaimNumber', 'LossState',
       'LossDate', 'NoticeDate',
       'ClaimStatus', 'ClosedDate', 'Coverage:', 'CoverageType', 'Loss Description', '',
       'SubmissionID']
    popular_table=popular_table[(popular_table['NoticeDate'] != "")]
    popular_table=popular_table[(popular_table['LossDate'] != "")]
    popular_table['TotalPaid'] = popular_table['CarrierClaimNumber'].shift(-1)
    popular_table['TotalGrossIncurred'] = popular_table['NoticeDate'].shift(-1)
    popular_table=popular_table[(popular_table['ClaimStatus'] != "")]
    return popular_table

def Axis1_Post_Processing(popular_table):

    popular_table.columns=['CarrierClaimNumber', 'LossState',
       'LossDate', 'NoticeDate',
       'ClaimStatus', 'ClosedDate', 'Coverage:', 'CoverageType', 'Loss Description', '',
       'SubmissionID']
    popular_table=popular_table[(popular_table['NoticeDate'] != "")]
    popular_table=popular_table[(popular_table['LossState'] != "")]
    popular_table['TotalPaid'] = popular_table['CarrierClaimNumber'].shift(-1)
    popular_table['TotalGrossIncurred'] = popular_table['NoticeDate'].shift(-1)
    popular_table=popular_table[(popular_table['ClaimStatus'] != "")]
    return popular_table

def Axis2_Post_Processing(popular_table):

    popular_table.columns=['CarrierClaimNumber', 'LossState',
       'LossDate', 'NoticeDate',
       'ClaimStatus', 'ClosedDate', 'Coverage:', 'CoverageType', 'Loss Description', '',
       'SubmissionID']
    popular_table=popular_table[(popular_table['NoticeDate'] != "")]
    popular_table=popular_table[(popular_table['LossDate'] != "")]
    popular_table=popular_table[(popular_table['CarrierClaimNumber'] != "")]
    popular_table['TotalPaid'] = popular_table['CarrierClaimNumber'].shift(-1)
    popular_table['TotalGrossIncurred'] = popular_table['NoticeDate'].shift(-1)
    popular_table=popular_table[(popular_table['ClaimStatus'] != "")]
    return popular_table


def Employers_Post_Processing(popular_table):

    popular_table.columns=['Injured Employee', 'CarrierClaimNumber', 'Fatalities', 'LossDate',
       'ClaimStatus', 'CoverageType', 'Body Part', 'Injury Type', 'Class',
       'Medical Paid', 'Medical Reserve', '', 'Indemnity Paid',
       'Indemnity Reserve', 'Recovery', '', 'Deductible', 'TotalGrossIncurred',
       'Net Expense', 'SubmissionID']
    popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Paid'] + x['Indemnity Paid'], axis=1)
    popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Reserve'] + x['Indemnity Reserve'], axis=1)
    popular_table=popular_table[(popular_table['TotalGrossIncurred']!='') & (popular_table['CarrierClaimNumber']!='')]
    return popular_table


def Employers1_Post_Processing(popular_table):

    popular_table.columns=['Injured Employee', 'CarrierClaimNumber', 'Fatalities', 'LossDate',
       'ClaimStatus', 'CoverageType', 'Body Part', 'Injury Type', 'Class',
       'Medical Paid', 'Medical Reserve', '', 'Indemnity Paid',
       'Indemnity Reserve', 'Recovery', '', 'Deductible', 'TotalGrossIncurred',
       'Net Expense', 'SubmissionID']
    popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Paid'] + x['Indemnity Paid'], axis=1)
    popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Reserve'] + x['Indemnity Reserve'], axis=1)
    popular_table=popular_table[(popular_table['TotalGrossIncurred']!='') & (popular_table['CarrierClaimNumber']!='')]
    return popular_table


def Tokio_Marine_Post_Processing(popular_table):
    popular_table.columns=['LineOfBusiness:',
       'Claim','PolicyPeriodStart','PolicyPeriodEnd', 
       'LossDate','LossState', 'TotalPaid',
       'Indemnity Description Paid', 'Expenses  Paid', '', 'SubmissionID']
    popular_table['CarrierPolicyNumber']=popular_table['Claim'].shift(-1)
    popular_table['ValuationDate']=popular_table['PolicyPeriodEnd'].shift(-6)
    popular_table=popular_table[popular_table['CarrierPolicyNumber'].apply(lambda x:('$' not in x) if len(str(x))==10 else False)]
    popular_table=popular_table[(popular_table['PolicyPeriodEnd']!='') & (popular_table['TotalPaid']!='')]
    return popular_table