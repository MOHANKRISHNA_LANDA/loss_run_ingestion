import pandas as pd
# import pdb; pdb.set_trace()
from fuzzywuzzy import fuzz
import traceback
import sys
from carrier_specific_extraction.carrier_specific_codes import Carrier_mapper 

class Postprocess_module():

    def __init__(self):

        self.original_cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
           'PolicyPeriodEnd', 'LossDate', 'ClaimStatus','TotalReserve','TotalPaid',
           'TotalGrossIncurred','TotalRecoveries','ValuationDate', 'CarrierName',
            'NoticeDate','ClosedDate','CarrierPolicyNumber','CoverageType','LossState',
            'LossDescription','PolicyYear','LocationOfProperty','LossBasis','LossCause','Peril', 'Submission_ID']
            
    def clean_header(self, all_tables):
        """
        """
        clean_tables=[] ; table_headers=[]
        for table in all_tables:
            try : 
                columns=[column.strip() for column in table.columns]
            except AttributeError : 
                columns = table.columns
            table_headers.append(str(columns))
            table.columns=columns
            clean_tables.append(table)
        
        return table_headers, clean_tables

    def post_process(self, schema_mapping, table_headers, clean_tables, carrier_name="other", switch_track=False, threshold=100):
        """
        """
        if switch_track is True : 
            results = Carrier_mapper.get_carrier(carrier_name, clean_tables)
            return results

        results=[]
        abc_error_stack=[]
        headers_vc = pd.Series(table_headers).value_counts()
        
        for index, popular_count in enumerate(headers_vc):
            popular_schema = str(eval(list(headers_vc.reset_index()['index'])[index]))
            print('Schema Number',index)
            schemas=list(schema_mapping['Schema'])

            #Comparing each found schema with stored list
            found = False
            for schema in schemas:
                if eval(schema)==eval(popular_schema):
                    print('Direct match')
                    carrier_name=schema_mapping[schema_mapping['Schema'] == schema]['CarrierName'].values[0]
                    found=True
                    break
                if fuzz.ratio(schema,popular_schema)>threshold and (schema.count(',')==popular_schema.count(',')):
                    print('Fuzzy match',fuzz.ratio(schema,popular_schema))
                    found=True
                    break
            popular_schema = eval(popular_schema)

            #collecting most frequent table 
            Merged_table = pd.DataFrame(columns = popular_schema)
            for table in clean_tables:
                if list(table.columns)==popular_schema:
                    Merged_table = pd.concat([Merged_table,table])

            if found:
                result = self.mapping_extaction(carrier_name, Merged_table)
                results.append(result)

        return results
    
    def mapping_extaction(self, carrier_name, Merged_table):
        """
        """
        try:
            result = Carrier_mapper.get_carrier(carrier_name, Merged_table)
            result['CarrierName']=carrier_name
            Merged_table['CarrierName']=carrier_name
            return result 
        except  Exception as e:
            exc_info = sys.exc_info()
            print("exc_info", exc_info)
            print("traceback.format_exc()-->",traceback.format_exc())
            print('Post Processing Error',e.with_traceback)
            return []

    def master_table(self, postprocessed_tables):
        """
        """
        Master_table = pd.DataFrame(columns = self.original_cols)
        try : 
            for table in postprocessed_tables : 
                present_columns = set(table.columns).intersection(self.original_cols)
                Master_table = pd.concat([Master_table, table[present_columns]])
        except Exception as e : 
            pass 
        return Master_table