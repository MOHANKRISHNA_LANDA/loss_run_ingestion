from camelot import read_pdf 
from tqdm import tqdm
from sys import exit
import pickle
import helper_functions.normalize_heuristic as hr
from glob import glob

class Table_extractor : 

    def __init__(self,input_folder) : 
        self.parameter_dictionary = {'pages':"all", 'flavor':"stream", 'split_text':False, 'muliple_tables':False } 
        self.input_folder = input_folder

    def read_doc(self, input_folder) :
        """
        """
        filenames = glob(input_folder+"/*.pdf")
        return filenames

    def extractor(self, filenames, prms_dict, use_slicer=True, save_pickle=False):
        """
        """
        prms_dict = self.Merge(self.parameter_dictionary, prms_dict)
        all_tables=[]
        for filename in tqdm(filenames):
            try:
                df_cam = read_pdf(self.input_folder + filename + ".pdf", flavor=prms_dict['flavor'], pages=prms_dict['pages'], 
                                muliple_tables = prms_dict['muliple_tables'], split_text = prms_dict['split_text']
                                )
            except KeyboardInterrupt:
                print ('Manual Interrupt')
                exit()
            except Exception as e:
                print(f'Exception occured while reading the table : {e}')
                continue 
                   
            all_tables = self.table_slicer(df_cam, filename, all_tables, use_slicer)

        if save_pickle is True : 
            with open(self.input_folder + "/extracted_tables.pkl","wb") as fp : 
                pickle.dump(all_tables, fp)
        
        return all_tables
    
    def Merge(self, dict1, dict2): 
        """
        """
        res = {**dict1, **dict2} 
        return res 
      

    def table_slicer(self, df_cam, filename, all_tables, use_slicer):
        """
        """
        for i in range(df_cam.n):
            table_=df_cam[i].df
            try:
                if use_slicer is False : 
                    sliced_tables = [table_]
                else :    
                    sliced_tables = hr.slice_multiple_tables(hr.clean_table(table_))
                
                for tab in sliced_tables:
                    if len(tab)>1:
                        tab['Submission_ID']=filename
                        all_tables.append(tab)
            except Exception as e:
                print(f'Exception occured while doing initial preprocessing of the table : {e}')
                pass  
        return all_tables