import sys
sys.path.extend(["/home/predator/OFFICE/loss_run_ingestion/"])
from helper_functions import camelot_extractor , Postprocess_module
from carrier_specific_codes import *
from carrier_identification import get_carrier
import pandas as pd 
import time
import warnings
warnings.filterwarnings("ignore")


def Main_func() : 
    input_folder = '../../chubb-lr-track/Testing_folder/'
    Ci_ = get_carrier.Carrier_Identification()
    Td_ = camelot_extractor.Table_extractor(input_folder)
    pp_ = Postprocess_module.Postprocess_module()
    filenames = Td_.read_doc(input_folder)
    schema_mapping=pd.read_csv('../Carrier_Schema_V1.csv')
    distribution_dict = Ci_.CI_Main(filenames)    
    parameters_dict = {
        'Hartford' : [{'pages':"all", 'flavor':"lattice", 'split_text':True, 'muliple_tables':True},True,False] ,
        'Hanover'  : [{},True,True],
        'other'    : [{},False,True]}

    Master_table = pd.DataFrame()
    distribution_dict = {'Hartford': ['P418110217553078', 'P318123113242156']}
    print(distribution_dict)
    for key in distribution_dict.keys() :
        tables = Td_.extractor( distribution_dict[key], parameters_dict[key][0], use_slicer=parameters_dict[key][2], save_pickle=True)
        table_headers, clean_tables = pp_.clean_header(tables)        
        results = pp_.post_process(schema_mapping, table_headers, clean_tables, carrier_name=key, switch_track=parameters_dict[key][1], threshold=100)
        Master_table = pd.concat([Master_table, pp_.master_table(results)])

    Master_table.to_csv('../../chubb-lr-track/Testing_folder/Master_table1.csv')



if __name__ == "__main__" : 
    Main_func()