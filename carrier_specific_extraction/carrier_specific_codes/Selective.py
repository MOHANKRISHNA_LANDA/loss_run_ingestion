import pandas as pd 
import numpy as np 

class Selective():

    def __init__(self):
        print("", end="")

    def Selective(self, pop):
        #filtering the rows
        pop['payment_there']=pop['Loss  Payments'].apply(lambda x:('$' in x) or (type(x)==float) if type(x)==str else False )
        popular_table=pop[pop['payment_there']]
        #renaming columns
        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID']
        popular_table.columns=['LineOfBusiness', 'Loss  Reserves', 'TotalGrossIncurred',
            'Expense  Payments', 'Loss  Recoveries', 'Loss  Deductibles',
            'Expense  Deductibles', 'Submission_ID', 'payment_there']
    
        found_cols=set(popular_table.columns).intersection(cols)
        popular_table.index=range(len(popular_table))
        return popular_table[found_cols]

    def Selective1(self, pop):
        #filtering the rows
        pop['payment_there']=pop['Loss  Payments'].apply(lambda x:('$' in x) or (type(x)==float) if type(x)==str else False )
        popular_table=pop[pop['payment_there']]
        #renaming columns
        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID']
        popular_table.columns=['LineOfBusiness', 'Loss  Reserves', 'TotalGrossIncurred',
            'Expense  Payments', 'Loss  Recoveries', 'Loss  Deductibles',
            'Expense  Deductibles','', 'Submission_ID', 'payment_there' ]
    
        found_cols=set(popular_table.columns).intersection(cols)
        popular_table.index=range(len(popular_table))
        return popular_table[found_cols]


    def Selective2(self, pop):
        #filtering the rows
        pop['payment_there']=pop['Loss  Payments'].apply(lambda x:('$' in x) or (type(x)==float) if type(x)==str else False )
        popular_table=pop[pop['payment_there']]
        #renaming columns
        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID']
        popular_table.columns=['LineOfBusiness', 'Loss  Reserves', 'TotalGrossIncurred',
            'Expense  Payments', 'Loss  Recoveries', 'Loss  Deductibles',
            'Expense  Deductibles','Submission_ID', 'payment_there' ]
    
        found_cols=set(popular_table.columns).intersection(cols)
        popular_table.index=range(len(popular_table))
        return popular_table[found_cols]
