import pandas as pd 
import numpy as np 

class Employers():

    def __init__(self):
        print("", end="")

    def Employers(self, popular_table):

        popular_table.columns=['Injured Employee', 'CarrierClaimNumber', 'Fatalities', 'LossDate',
        'ClaimStatus', 'CoverageType', 'Body Part', 'Injury Type', 'Class',
        'Medical Paid', 'Medical Reserve', '', 'Indemnity Paid',
        'Indemnity Reserve', 'Recovery', '', 'Deductible', 'TotalGrossIncurred',
        'Net Expense', 'Submission_ID']
        popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Paid'] + x['Indemnity Paid'], axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Reserve'] + x['Indemnity Reserve'], axis=1)
        popular_table=popular_table[(popular_table['TotalGrossIncurred']!='') & (popular_table['CarrierClaimNumber']!='')]
        return popular_table


    def Employers1(self, popular_table):

        popular_table.columns=['Injured Employee', 'CarrierClaimNumber', 'Fatalities', 'LossDate',
        'ClaimStatus', 'CoverageType', 'Body Part', 'Injury Type', 'Class',
        'Medical Paid', 'Medical Reserve', '', 'Indemnity Paid',
        'Indemnity Reserve', 'Recovery', '', 'Deductible', 'TotalGrossIncurred',
        'Net Expense', 'Submission_ID']
        popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Paid'] + x['Indemnity Paid'], axis=1)
        popular_table['TotalPaid'] = popular_table.apply(lambda x: x['Medical Reserve'] + x['Indemnity Reserve'], axis=1)
        popular_table=popular_table[(popular_table['TotalGrossIncurred']!='') & (popular_table['CarrierClaimNumber']!='')]
        return popular_table