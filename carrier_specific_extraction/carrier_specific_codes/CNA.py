import pandas as pd 
import numpy as np

class CNA():

    def __init__(self):
        print("", end="")

    def CNA(self, popular_table):
        popular_table.columns=['NoticeDate', 'LossDate', "Occur Date",'ClosedDate', 'CarrierClaimNumber',
        'ClaimStatus', 'LossState', 'Location State', 'im', 'TotalPaid',
        'Total Expenses','Incurred', 'TotalGrossIncurred', 'Submission_ID']
    #     popular_table['LossDescription'] = popular_table['ClosedDate'].shift(-1)
        popular_table=popular_table[popular_table['TotalGrossIncurred'] != ""]
        return popular_table

    def CNA2(self, popular_table):
        popular_table.columns=['NoticeDate', 'LossDate','Occur Date', 'ClosedDate', 'CarrierClaimNumber',
        'ClaimStatus','Accident Claimant Name', 'LossState', 'Indemnity Claimant Name', 'TotalPaid', 'Indemnity  Paid',
        'Total Expenses', 'TotalGrossIncurred',  'Submission_ID']
        popular_table['LossDescription'] = popular_table['CarrierClaimNumber'].shift(-1)
        popular_table=popular_table[popular_table['TotalGrossIncurred'] != ""]
        wrg_claim = popular_table[((popular_table['ClaimStatus'] !="OPEN") & (popular_table['ClaimStatus'] !="CLOSED"))]
        wrg_claim.columns=['NoticeDate', 'LossDate','Occur Date', 'ClosedDate', 'CarrierClaimNumber',
        'Claim Status','ClaimStatus', 'LossState', 'Indemnity Claimant Name', 'TotalPaid', 'Indemnity  Paid',
        'Total Expenses', 'TotalGrossIncurred',  'Submission_ID','LossDescription']
        result = pd.concat([popular_table,wrg_claim])
        result = result[((result['ClaimStatus'] =="OPEN") | (result['ClaimStatus'] =="CLOSED"))]
        return result

    def CNA3(self, popular_table):
        popular_table.columns=['NoticeDate', 'LossDate', 'ClosedDate', 'CarrierClaimNumber',
        'ClaimStatus', 'LossState', 'Location State','Indemnity Claimant Name', 'TotalPaid', 'Indemnity  Paid',
        'Total Expenses', 'TotalGrossIncurred', 'Incurred', 'Submission_ID']
        popular_table['LossDescription'] = popular_table['ClosedDate'].shift(-1)
        tune_table=popular_table[((popular_table['CarrierClaimNumber'].str.len() !=8) & (popular_table['CarrierClaimNumber'] !=""))]
        tune_table.columns=['NoticeDate', 'LossDate', 'occur date', 'ClosedDate',
        'CarrierClaimNumber', 'ClaimStatus', 'LossState','Indemnity Claimant Name', 'Total Paid', 'TotalPaid',
        'Total Expenses', 'Total Incurred', 'TotalGrossIncurred', 'Submission_ID','LossDescription']
        result_final = pd.concat([popular_table,tune_table])
        result_final=result_final[((result_final['CarrierClaimNumber'].str.len() ==8) & (result_final['CarrierClaimNumber'] !=""))]
        wrg_claim = result_final[((result_final['ClaimStatus'] !="OPEN") & (result_final['ClaimStatus'] !="CLOSED"))]
        wrg_claim.columns=['NoticeDate', 'LossDate', 'ClosedDate', 'CarrierClaimNumber',
        'Claim Status', 'ClaimStatus', 'LossState', 'Indemnity Claimant Name',
        'TotalPaid', 'Indemnity  Paid', 'Total Expenses', 'TotalGrossIncurred',
        'Incurred', 'Submission_ID', 'LossDescription', 'occur date',
        'Total Paid', 'Total Incurred']
        result = pd.concat([result_final,wrg_claim])
        result = result[((result['ClaimStatus'] =="OPEN") | (result['ClaimStatus'] =="CLOSED"))]
        return result