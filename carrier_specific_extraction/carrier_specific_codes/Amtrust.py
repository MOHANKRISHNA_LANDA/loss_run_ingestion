import pandas as pd
import numpy as np
from dateutil.parser import parse

class Amtrust():

    def __init__(self):
        self.mapping_dict = {
            'Claim No':'Claim Number',
            'Policy Number':'Policy Number',
            'Pol. Eff Date':'Policy Period Start',
            'Insured':'Insured Name',
            'DOL':'Loss Date/ Accident Date',
            'Status':'Claim Status',
            'Category':'Line of Business',
            'Loss Description':'Loss Description',
            'Total1':'Total Paid',
            'Total2':'Total Reserve',
            'Total3':'Total Gross Incurred',
            'Total4':'Total Recoveries',
            'Juris St':'Loss State',
            'Date Rcvd':'Date Reported',
            'Cause':'Loss Cause',
            'Submission_ID': 'Submission_ID'
            }

    def Amtrust1(self, popular_table) : 
        """
        Input : Table dataframe belonging to the Amtrust Carrier Schema 1 
        """
        # mapping the column names in the file to the column names of the master dataframe file 
        master_df = pd.DataFrame(columns=self.mapping_dict.values())
        #tracker to find out the unique row items since the a single claim spans across mulitple rows 
        tracker = list(map(lambda x : 1 if x=='1' else 0,popular_table['Claim No Converted # Policy Number Pol. Eff Date'].values))
        tracker_ind = [ind for ind,val in enumerate(tracker) if val==1]
        tracker_ind.append(len(popular_table))
        data = [tracker_ind[n:n+2] for n in range(len(tracker_ind))]
        final_data = []
        counter = 0
        for x in data[:-1]:
            final_data.extend([counter]*(x[1]-x[0])) 
            counter+=1
        
        popular_table.insert(0,'tracker',final_data)
        popular_table.columns = [x if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
        popular_table = popular_table.groupby(by=['tracker'],as_index=False).agg('||'.join)
        # merging multiple row items column-wise by \\ to identify which item corresponds to which column 
        first_list = popular_table['Claim No Converted # Policy Number Pol. Eff Date'].map(lambda x:x.split('||')).values
        first_list = [[x for x in y if x] for y in first_list]
        second_list = popular_table[' Claimant Class Cd Juris St Insured'].map(lambda x:x.split('||')).values
        second_list = [[x for x in y if x] for y in second_list]
        third_list = popular_table[' Department DOL First Aware Date Rcvd'].map(lambda x:x.split('||')).values
        third_list = [[x for x in y if x] for y in third_list]
        fourth_list = popular_table['  Status Category Adjuster'].map(lambda x:x.split('||')).values
        fourth_list = [[x for x in y if x] for y in fourth_list]
        fifth_list = popular_table[' Loss Location Part Injured Cause Loss Description'].map(lambda x:x.split('||')).values
        fifth_list =[[x for x in y if x] for y in fifth_list]
        sixth_list = popular_table['    Total'].map(lambda x:x.split('||')).values
        sixth_list = [[x for x in y if x] for y in sixth_list]
        seventh_list = popular_table['Submission_ID'].map(lambda x:x.split('||')).values
        seventh_list = [[x for x in y if x] for y in seventh_list]
        eight_list = popular_table['NA_7'].map(lambda x:x.split('||')).values
        eight_list = [[x for x in y if x] for y in eight_list]
        nine_list = popular_table['NA_12'].map(lambda x:x.split('||')).values
        nine_list = [[x for x in y if x] for y in nine_list]
        
        def lob(fourth_list, fifth_list) : 
            """
            Function to extract line of business. 
            """
            lob_ = []
            for i in range(len(fourth_list)) : 
                if len(fourth_list[i][0].split()) != 1: 
                    if fifth_list[i][1].isupper() : 
                        lob_.append(fourth_list[i][0].split()[-1] + fifth_list[i][1])
                    else : 
                        lob_.append(fourth_list[i][0].split()[-1] + fifth_list[i][2])
                else : 
                    lob_.append(fifth_list[i][1])
            return lob_
        # dictionary with column names as keys mappend to their necessary function 
        dispatcher = {
            'Claim Number': lambda : [x[1] for x in first_list],
            'Policy Number': lambda : [x[-3][-1]+x[-2] for  x in first_list],
            'Policy Period Start' : lambda : [x[-1] for x in first_list],
            'Loss State' : lambda :  [x[2] for x in second_list],
            'Insured Name': lambda : [x[3] for x in second_list],
            'Loss Date/ Accident Date' : [x[0] for x in third_list],
            'Date Reported' : lambda : [x[2] for x in third_list],
            'Claim Status' : lambda :[x[0] for x in fourth_list],
            'Loss Cause' : lambda :[x[-2] for x in eight_list],
            'Loss Description' : lambda :[' '.join(x[2:]) for x in fifth_list],
            'Total Gross Incurred': lambda :[sixth_list[i][v] for i, v in enumerate([-1 if x[-1] == "Incurred" else x.index("Incurred") for x in eight_list ])],
            'Total Reserve' : lambda : [sixth_list[i][0] for i in range(len(sixth_list))],
            'Total Recoveries': lambda :[nine_list[i][-1] for i in range(len(nine_list))],
            'Total Paid' : lambda : [nine_list[i][0] for i in range(len(nine_list))],
            'Line of Business': lambda : lob(fourth_list, fifth_list),
            'Submission_ID': lambda : [x[0] for x in seventh_list],
            'fallback':lambda df : ['NA']*len(new_df)
            }
        # iterating over each column name and appending the row items 
        new_df = pd.DataFrame(columns=self.mapping_dict.values())
        for col in master_df.columns :
            try:
                new_df[col] = dispatcher[col]()
            except:
                new_df[col] = dispatcher['fallback'](new_df)
        master_df = pd.concat([master_df, new_df])
        master_df.index = range(len(master_df))
        master_df['Claim Status'] = master_df['Claim Status'].map(lambda x:x.split()[0])
        master_df['Submission_ID'] = master_df['Submission_ID'].map(lambda x:x.split('/')[-1].strip('.pdf'))
        return master_df

    def Amtrust2(self, popular_table) : 
        """
        Input : Table dataframe belonging to the Amtrust Carrier Schema 2 & 4 
        """
        # mapping the column names in the file to the column names of the master dataframe file 
        master_df = pd.DataFrame(columns=self.mapping_dict.values())
        def total_fields(term):
            """
            Function to extract all the total fields. 
            """
            fifth_list = []
            for i in range(popular_table.shape[0]) : 
                mets = np.array(list(map(lambda x:x.split('||'), popular_table.iloc[i].values[1:-1])))
                indice = np.argwhere(mets == term)[0][1]
                fifth_list.append(list(filter(lambda x : x.strip().replace(",","").isdigit(), mets[:,indice])))
            return list(map(lambda x: x[-1], fifth_list))

        def parse_(string, fuzzy = False) : 
            """
            Function to check if a string is a date.
            """
            try: 
                parse(string, fuzzy=fuzzy)
                return True
            except ValueError:
                return False

        def loss_desc(fourth_list) : 
            """
            Function to merge all the description strings.
            """
            dates = [x[-1] for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]]
            return [" ".join(fourth_list[i][fourth_list[i].index(dates[i])+1:]) for i in range(len(fourth_list))]

        
        popular_table.columns = [x if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
        
        #tracker to find out the unique row items since the a single claim spans across mulitple rows 
        tracker = list(map(lambda x : 1 if x=='1' else 0,popular_table['NA_1'].values))
        tracker_ind = [ind for ind,val in enumerate(tracker) if val==1]
        tracker_ind.append(len(popular_table))
        data = [tracker_ind[n:n+2] for n in range(len(tracker_ind))]
        final_data = []
        counter = 0
        for x in data[:-1]:
            final_data.extend([counter]*(x[1]-x[0])) 
            counter+=1
        popular_table.insert(0,'tracker',final_data)
        popular_table.columns = [x.strip() if x.strip() else f'NA_{ind}' for ind,x in enumerate(popular_table.columns)]
        popular_table = popular_table.groupby(by=['tracker'],as_index=False).agg('||'.join)
        
        # merging multiple row items column-wise by \\ to identify which item corresponds to which column 
        first_list = popular_table['Claim No Converted # Policy Number Pol. Eff Date'].map(lambda x:x.split('||')).values
        first_list = [[x for x in y] for y in first_list]
        second_list = popular_table['Claimant Class Cd Juris St Insured'].map(lambda x:x.split('||')).values
        second_list= [[x for x in y if x] for y in second_list]
        third_list = popular_table['Department DOL First Aware Date Rcvd'].map(lambda x:x.split('||')).values
        third_list = [[x for x in y if x] for y in third_list]
        fourth_list = popular_table['Loss Location Nature Employee Lag Reporting Lag'].map(lambda x:x.split('||')).values
        fourth_list = [[x for x in y if x] for y in fourth_list]
        seventh_list = popular_table['Submission_ID'].map(lambda x:x.split('||')).values
        seventh_list = [[x for x in y if x] for y in seventh_list]
        # dictionary with column names as keys mappend to their necessary function 
        dispatcher = {
            'Claim Number': lambda : [x[1] for x in first_list],
            'Policy Number': lambda : [x[2][-1]+x[3] if len(x[2].split()) > 1 else x[3] for x in first_list],
            'Policy Period Start' : lambda : [x[4] for x in first_list],
            'Loss State' : lambda :  [x[0] for x in second_list],
            'Insured Name': lambda : [x[3] for x in second_list],
            'Loss Date/ Accident Date' : [x[0] if len(x)>0 else "NA" for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]],
            'Date Reported' : lambda : [x[1] if len(x) > 1 else "NA" for x in [list(filter(lambda x : parse_(x), fourth_list[i])) for i in range(len(fourth_list))]],
            'Claim Status' : lambda :[x[1].split()[0] for x in third_list],
            'Line of Business' : lambda :[x[1].split()[1]+ x[2] if len(x[1].split()) > 1 else x[2] for x in third_list],
            'Loss Description' : lambda :loss_desc(fourth_list) , 
            'Total Gross Incurred': lambda :total_fields("Incurred"),
            'Total Reserve' : lambda :total_fields("Reserves") ,
            'Total Recoveries': lambda :total_fields("Recoveries") ,
            'Total Paid' : lambda :total_fields("Payments") ,
            'Submission_ID': lambda : [x[0] for x in seventh_list],
            'fallback':lambda df : ['NA']*len(new_df)
            }
        # iterating over each column name and appending the row items 
        new_df = pd.DataFrame(columns=self.mapping_dict.values())
        for col in master_df.columns :
            try:
                new_df[col] = dispatcher[col]()
            except:
                new_df[col] = dispatcher['fallback'](new_df)
        master_df = pd.concat([master_df, new_df])
        master_df.index = range(len(master_df))
        master_df['Claim Status'] = master_df['Claim Status'].map(lambda x:x.split()[0])
        master_df['Submission_ID'] = master_df['Submission_ID'].map(lambda x:x.split('/')[-1].strip('.pdf'))
        return master_df