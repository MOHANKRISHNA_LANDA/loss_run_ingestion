import pandas as pd
import numpy as np


class Zurich() : 
    
    def __init__(self):
        print("", end="")

    def Zurich(self, table_):
        """ """
        table_.columns=['CarrierClaimNumber', 'Date of Claimant','LossDate', 
            'LossState', 'ClaimStatus', 'NoticeDate',
            'Indemnity Date', 'ClosedDate', 'BI/Med', 'Exp PD/LT', 'Reserve',
            'Res','TotalPaid', 'TotalGrossIncurred', 'Paid', 'Submission_ID']
        
        table_=table_[table_['CarrierClaimNumber']!='']    
        table_['LossDescription'] = table_['Date of Claimant'].shift(-1)
        table_['CarrierClaimNumber'] = table_['CarrierClaimNumber'].str.extract('(\d+)([^a-zA-Z]+)', expand=True)[0]
        table_['CarrierClaimNumber']=table_['CarrierClaimNumber'].apply(lambda x:x if len(str(x)) == 10  else '')
        table_=table_[table_['CarrierClaimNumber']!=''] 
        return table_

    def Zurich1(self, table_):
        """ """
        table_['CarrierClaimNumber'] = table_['Loss Claimant'].str.extract('(\d{10})', expand=True)
        table_=table_[table_['Total Paid']!=''] 
        table_.columns=['Coverage Type', 'Loss Claimant', 'LossDate',
        'LossState', 'ClaimStatus', 'NoticeDate', 'ClosedDate',
        'Date', 'Exp BI/Med', 'PD/LT', 'Incurred Reserve', 'Exp Paid', 'TotalPaid',
        'TotalGrossIncurred', 'Total', 'Submission_ID', 'CarrierClaimNumber']
        return table_

    def Zurich2(self, table_):
        """ """
        table_['CarrierClaimNumber'] = table_['Loss State'].str.extract('(\d{10})', expand=True)
        table_=table_[table_['Total Paid']!=''] 
        table_.columns=['Coverage Type', 'Loss Claimant', 'LossDate',
        'LossState', 'ClaimStatus', 'NoticeDate', 'ClosedDate',
        'Date', 'Exp BI/Med', 'PD/LT', 'Incurred Reserve', 'Exp Paid', 'TotalPaid',
        'TotalGrossIncurred', 'Total', 'Submission_ID', 'CarrierClaimNumber']
        return table_