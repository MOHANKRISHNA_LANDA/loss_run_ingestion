import pandas as pd 
import numpy as np 

class Tokio_Marine():

    def __init__(self):
        print("", end="")

    def Tokio_Marine(self, popular_table):
        popular_table.columns=['LineOfBusiness:',
        'Claim','PolicyPeriodStart','PolicyPeriodEnd', 
        'LossDate','LossState', 'TotalPaid',
        'Indemnity Description Paid', 'Expenses  Paid', '', 'Submission_ID']
        popular_table['CarrierPolicyNumber']=popular_table['Claim'].shift(-1)
        popular_table['ValuationDate']=popular_table['PolicyPeriodEnd'].shift(-6)
        popular_table=popular_table[popular_table['CarrierPolicyNumber'].apply(lambda x:('$' not in x) if len(str(x))==10 else False)]
        popular_table=popular_table[(popular_table['PolicyPeriodEnd']!='') & (popular_table['TotalPaid']!='')]
        return popular_table