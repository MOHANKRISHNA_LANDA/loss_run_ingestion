# Loads all the individual carrier functions and maps them to a dictionary and return the desired one 


import pandas as pd
from . import Amtrust, Cincinnati , Liberty, Hanover, AIG, Axis, Berkeley, CNA, Employers, Firemans_Fund, nationwide, onebeacon, Selective, Tokio_Marine, Travelers, Zurich , Hartford
import re

def get_carrier(carrier_name, Merged_table) : 
    map_dict = {
    "Amtrust" : initialize_object(Amtrust.Amtrust),
    "AIG" : initialize_object(AIG.AIG),
    "Axis" : initialize_object(Axis.Axis),
    "Berkeley" : initialize_object(Berkeley.Berkeley),
    "CNA" : initialize_object(CNA.CNA),
    "Cincinnati" : initialize_object(Cincinnati.Cincinnati),
    "Employers" : initialize_object(Employers.Employers),
    "Firemans_Fund" : initialize_object(Firemans_Fund.Firemans_Fund),
    "Hanover" : initialize_object(Hanover.Hanover),
    "Hartford" : initialize_object(Hartford.Hartford), 
    "Liberty" : initialize_object(Liberty.Liberty),
    "nationwide" : initialize_object(nationwide.nationwide),
    "onebeacon" : initialize_object(onebeacon.onebeacon),
    "Selective" : initialize_object(Selective.Selective),
    "Tokio_Marine" : initialize_object(Tokio_Marine.Tokio_Marine),
    "Travelers" : initialize_object(Travelers.Travelers),
    "Zurich" : initialize_object(Zurich.Zurich)
        }
    try : 
        results = getattr(map_dict[re.match(r'[A-Za-z]*',carrier_name).group()], carrier_name)(Merged_table)
        return results
    except AttributeError : 
        return []

def initialize_object(class_name) : 
    class_obj = class_name()
    return class_obj