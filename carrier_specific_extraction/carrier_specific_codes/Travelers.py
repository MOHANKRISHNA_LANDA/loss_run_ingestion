import pandas as pd 
import numpy as np 


class Travelers():

    def __init__(self):
        print("", end="")

    def Travelers(self, df):
        df['LineOfBusiness'] = df['Claimant'].apply(lambda x:  x.split('Line of Insurance:')[1] if (type(x)==str and 'Line of Insurance:' in x) else None ).shift(1)
        df['Policy Number'] = df['Claimant'].apply(lambda x:  x.split('Policy Year:')[1] if (type(x)==str and 'Policy Year:' in x) else None ).shift(2)
        df['ClaimStatus'] = df['O/C'].apply(lambda x:  x if (x =='C' or x=='O' ) else 'None' )
        df.columns=['Claimant', 'Adj Off', 'FP', 'CarrierClaimNumber', 'LossDate',
        'NoticeDate', 'ClosedDate', 'O/C', 'TotalGrossIncurred', 'Claim', 'Medical',
        'Expense',       'Claimant Adj OfFPCarrierClaimNumber Expense',
        'Submission_ID', 'LineOfBusiness', 'Policy Number','ClaimStatus']

        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID','Policy Number','NoticeDate','ClosedDate']
        
        found=set(df.columns).intersection(set(cols))
        df=df[found]
        df['TotalGrossIncurred'] = df['TotalGrossIncurred'].apply(lambda x:  x if (type(x)==str and '$' in x)else "None" )
        df=df[((df['TotalGrossIncurred']!='None'))]
    #     df['CarrierClaimNumber'] = df['CarrierClaimNumber'].apply(lambda x: x).replace('', np.nan)
    #     df['CarrierClaimNumber'].fillna(method='ffill',limit = 2, inplace=True)
        df.index=range(len(df))
        return df
    

    def Travelers1(self, df):
        df['LineOfBusiness'] = df['Claimant Line of Insurance: WC - WORKERS COMP'].apply(lambda x:  x.split('Claimant Line of Insurance:')[1] if (type(x)==str and 'Claimant Line of Insurance:' in x) else None ).shift(2)
    #     print(df)
        df['Policy Number'] = df['Claimant Line of Insurance: WC - WORKERS COMP'].apply(lambda x:  x.split('Policy Year:')[1] if (type(x)==str and 'Policy Year:' in x) else None ).shift(2)
        df['ClaimStatus'] = df['O/C'].apply(lambda x:  x if (x =='C' or x=='O' ) else 'None' )
        df.columns=['Claimant', 'Adj Off', 'FP', 'CarrierClaimNumber', 'LossDate',
        'NoticeDate', 'ClosedDate', 'O/C', 'TotalGrossIncurred', 'Claim', 'Medical',
        'Expense',
        'Claimant Adj OfFPCarrierClaimNumber Expense',
        'Submission_ID', 'LineOfBusiness', 'Policy Number','ClaimStatus']

        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID','Policy Number','NoticeDate','ClosedDate']
        
        found=set(df.columns).intersection(set(cols))
        df=df[found]
        df['TotalGrossIncurred'] = df['TotalGrossIncurred'].apply(lambda x:  x if (type(x)==str and '$' in x)else "None" )
        df=df[((df['TotalGrossIncurred']!='None'))] 
        df.index=range(len(df))
        return df
