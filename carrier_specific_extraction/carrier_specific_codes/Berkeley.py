import pandas as pd 
import numpy as np 

class Berkeley():

    def __init__(self):
        print("", end="")

    def Berkeley1(self, popular_table ):
        popular_table=popular_table[popular_table['Claimant Name Open Date Date']!='']
        popular_table.columns=['PolicyPeriodStart', 'PolicyPeriodEnd',
            'ClaimStatus',
            'Claimant Name Open Date Date', 'Claim Closed', 'Claim',
            'CarrierClaimNumber', 'Product Paid', 'TotalGrossIncurred',
            'Total Defense', '', '', 'Submission_ID']

        new_cols=['CarrierClaimNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd', 'LossDate',
            'TotalGrossIncurred',
        'ClaimStatus', 'LineOfBusiness', 'InsuredName', 'ValuationDate', 'Submission_ID']

        found_cols=set(popular_table.columns).intersection(new_cols)

        popular_table.index=range(len(popular_table))
        return popular_table[found_cols]

