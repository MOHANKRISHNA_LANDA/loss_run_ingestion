import pandas as pd 
import numpy as np 


class onebeacon():

    def __init__(self):
        print("", end="")

    def onebeacon(self,pop):
        pop.columns = ['ClaimStatus', 'Loss Claimant', 'Loss Age Paid $',
        'CarrierClaimNumber', 'Lgl Exp Pd   Pd $', 'TotalPaid',
        'Subro/  Rsv $ Rcvd$', 'Ded Amt', '', '', '', 'TotalGrossIncurred', 'Submission_ID']
        pop['CoverageType'] = pop['ClaimStatus'].shift(-1)
        pop=pop[pop['TotalGrossIncurred']!=''] 

        new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
                'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
                'ValuationDate', 'Submission_ID']

        found=list(set(pop.columns).intersection(set(new_cols)))
        phase1=pop[found]
        return phase1

    def onebeacon1(self,pop):
        pop.columns = ['ClaimStatus', 'CarrierClaimNumber', 'Alloc Exp  Age Paid $',
        'Loss  Rsv $ $', 'Lgl Exp Pd Rsv $ Pd $', 'TotalPaid', 'Subro/',
        'Total $ Rcvd$', 'Ded Amt', '', 'TotalGrossIncurred', '', 'Submission_ID']
        pop['CoverageType'] = pop['CarrierClaimNumber'].shift(-1)
        pop=pop[pop['TotalGrossIncurred']!=''] 

        new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
                'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
                'ValuationDate', 'Submission_ID']

        found=list(set(pop.columns).intersection(set(new_cols)))
        phase1=pop[found]
        return phase1