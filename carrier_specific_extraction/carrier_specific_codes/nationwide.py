import pandas as pd 
import numpy as np 


class nationwide():

    def __init__(self):
        print("", end="")

    def nationwide(self, pop):
        pop.columns = ['Type Premises Claim # ID Loss', 'Date of Location Loss',
        'LossDate', 'NoticeDate',
        'TotalPaid', 'Closed of Subro', 'TotalGrossIncurred', '','Loss Cause','CoverageType', '',
        'Submission_ID']
        pop['CarrierClaimNumber'] = pop['Type Premises Claim # ID Loss'].shift(1)
        pop['ClaimStatus'] = pop['Type Premises Claim # ID Loss'].shift(-1)
        pop['LossDescription'] = pop['Type Premises Claim # ID Loss'].shift(-2)
        pop['LossState'] = pop['Date of Location Loss'].shift(-1)

        pop=pop[pop['TotalGrossIncurred'].apply(lambda x:('$' in x) if type(x)==str else False)] 
        pop=pop[pop['CarrierClaimNumber'].apply(lambda x:('$' not in x) if len(x)>18 else False)]
        
        new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
                'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
                'ValuationDate', 'Submission_ID','LossState','CoverageType','Loss Cause']

        found=list(set(pop.columns).intersection(set(new_cols)))
        phase1=pop[found]
        return phase1

    def nationwide1(self, pop):
    
        pop.columns = ['Type Premises Claim # ID Loss', 'Date of Location Loss',
        'LossDate', 'NoticeDate',
        'TotalPaid', 'Closed of Subro', 'TotalGrossIncurred','Loss Cause','CoverageType', '', '',
        'Submission_ID']
        pop['CarrierClaimNumber'] = pop['Type Premises Claim # ID Loss'].shift(1)
        pop['ClaimStatus'] = pop['Type Premises Claim # ID Loss'].shift(-1)
        pop['LossDescription'] = pop['Type Premises Claim # ID Loss'].shift(-2)
        pop['LossState'] = pop['Date of Location Loss'].shift(-1)

        pop=pop[pop['TotalGrossIncurred'].apply(lambda x:('$' in x) if type(x)==str else False)] 
        pop=pop[pop['CarrierClaimNumber'].apply(lambda x:('$' not in x) if len(x)>18 else False)]
        
        new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
                'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
                'ValuationDate', 'Submission_ID','LossState','Loss Cause','CoverageType']

        found=list(set(pop.columns).intersection(set(new_cols)))
        phase1=pop[found]
        return phase1

    def nationwide2(self, pop):
        pop.columns = ['Type Premises Claim # ID Loss', 'LossDate',
        'NoticeDate', 'TotalPaid', 'Closed Incurred Subro',
        'TotalGrossIncurred', 'CoverageType', '', '', 'Submission_ID']
        pop['CarrierClaimNumber'] = pop['Type Premises Claim # ID Loss'].shift(2)
        pop['ClaimStatus'] = pop['LossDate'].shift(-2)
        pop['LossState'] = pop['Type Premises Claim # ID Loss'].shift(-3)
        pop['LossDescription'] = pop['Type Premises Claim # ID Loss'].shift(-4)
        pop['Loss Cause'] = pop['Type Premises Claim # ID Loss'].shift(1)

        pop=pop[pop['TotalGrossIncurred'].apply(lambda x:('$' in x) if type(x)==str else False)] 
        pop=pop[pop['CarrierClaimNumber'].apply(lambda x:('$' not in x) if len(x)>18 else False)]

        new_cols=['CarrierClaimNumber', 'PolicyPeriodStart','LossDate','Loss Type' ,'PolicyPeriodEnd',
                'TotalGrossIncurred','CoverageType','ClaimStatus', 'LineOfBusiness', 'InsuredName','TotalPaid',
                'ValuationDate', 'Submission_ID','LossState','NoticeDate']

        found=list(set(pop.columns).intersection(set(new_cols)))
        phase1=pop[found]
        return phase1
