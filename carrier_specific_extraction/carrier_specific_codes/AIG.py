import pandas as pd
import numpy as np

class AIG():

    def __init__(self):
        print("", end="")
        
    def AIG(self, popular_table):
        popular_table['CarrierClaimNumber']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].shift(1)
        popular_table['ClaimStatus']=popular_table['Div / H.O. Status Closed Date'].shift(1)
        popular_table['LossState']=popular_table['Loss State Receipt Date'].shift(1)
        popular_table['LossDescription']=popular_table['Currency:   Loss Description'].shift(2)
    
        popular_table.columns=['LossDate',
        'NoticeDate', 'ClosedDate',
        'Adjuster Name Manager Name', 'Currency:   Loss Description',
        'TotalPaid', 'Submission_ID', 'CarrierClaimNumber',
        'ClaimStatus','LossState','LossDescription']
        
        popular_table=popular_table[popular_table['TotalPaid'] !=""]        
        return popular_table

    def AIG1(self, popular_table):
        popular_table['CarrierClaimNumber']=popular_table['Claimant Name Claim # / OneClaim # Loss Date'].shift(1)
        popular_table['ClaimStatus']=popular_table['Status Closed Date'].shift(1)
        popular_table['LossState']=popular_table['Div / H.O. Loss State Receipt Date'].shift(1)
        popular_table['LossDescription']=popular_table['Currency:   Loss Description'].shift(1)

        popular_table.columns=['LossDate',
        'NoticeDate', 'ClosedDate',
        'Adjuster Name Manager Name', 'Currency:   Loss Description','USD',
        'TotalPaid', 'Submission_ID', 'CarrierClaimNumber',
        'ClaimStatus','LossState','LossDescription']
        
        popular_table=popular_table[popular_table['TotalPaid'] !=""]
        return popular_table