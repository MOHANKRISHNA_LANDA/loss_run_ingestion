import pandas as pd 
from dateutil.parser import parse
import re

class Hartford() : 

    def __init__(self):
        self.m_columns = ['LossDate','NoticeDate','ClosedDate','CarrierClaimNumber','RiskState','ClassCode','TotalPaid','PaidExpenses','OpenLosses',
    'OpenExpenses','TotalGrossIncurred', '#', 'Claimant/LocationOfProperty','Age','LenEmpl','AccCode','LossDescription','Submission_ID','LineOfBusiness',
    'CarrierPolicyNumber', 'PolicyPeriodStart', 'PolicyPeriodEnd' ] 

    def intialize(self, tables) :
        df = pd.DataFrame(columns = ['id','column_length'])
        for i, v in enumerate(tables) : 
            df.loc[df.shape[0]] = [i, v.shape[1]]
        index_list = df[df['column_length'] == 18].id.tolist()
        return index_list

    def parse_sub(self, string, fuzzy = False) : 
        try: 
            parse(string, fuzzy=fuzzy)
            return True
        except ValueError:
            return False

    def three_field_extractor(self, string): 
        string = re.sub("\n", "", string)
        if "Policy" in string and "Policy Term" in string : 
            LOB = string.split("Policy:")[0]
            Policy_Number = string.split("Policy:")[1].split("Policy Term")[0]
            Policy_Period   = string.split("Policy Term:")[1]
            PolicyPeriodStart = Policy_Period.split("-")[0]
            PolicyPeriodEnd = Policy_Period.split("-")[1]
            return LOB, Policy_Number, PolicyPeriodStart, PolicyPeriodEnd
        else : 
            return None , None, None , None 

    def Hartford(self, tables): 
        
        index_list = self.intialize(tables)
        if index_list.__len__() > 0 : 
            LOB = ""
            Policy_Number =""
            PolicyPeriodStart = ""
            PolicyPeriodEnd = ""
            master_table = pd.DataFrame()
            for index in index_list : 
                tab_ = tables[index]
                # tab_["Submission_ID"] = tab_["Submission_ID"].apply(lambda x : x.split("/")[-1].strip(".pdf"))
                LOB, Policy_Number, PolicyPeriodStart, PolicyPeriodEnd = self.three_field_extractor(tab_[0][0])
                true_indexes = [*filter(tab_[0].apply(lambda x : self.parse_sub(x)).get, tab_[0].apply(lambda x : self.parse_sub(x)).index)]
                if LOB is not None : 
                    tab_['LOB'] = LOB
                    tab_['Policy_Number'] = Policy_Number
                    tab_['PolicyPeriodStart'] = PolicyPeriodStart
                    tab_['PolicyPeriodEnd'] = PolicyPeriodEnd

                master_table = pd.concat([master_table, tab_.loc[true_indexes]])
            if master_table.shape[0] == 0 : 
                return []

            master_table.columns = self.m_columns
            master_table['CarrierName'] = "Hartford"

            return [master_table]

        else : 
            []
