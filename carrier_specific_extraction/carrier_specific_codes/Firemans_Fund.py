import pandas as pd 
import numpy as np 

class Firemans_Fund():

    def __init__(self):
        print("", end="")
        

    def Firemans_Fund(self, popular_table):
    
        popular_table=popular_table[~((popular_table['CarrierClaimNumber Status'] =='') & (popular_table['Loss Amount'] == '') )]
        popular_table['CarrierClaimNumber']=popular_table['CarrierClaimNumber Status'].shift(1)
        try:
            popular_table=popular_table[(popular_table['Description    Coverage'] =='')]
        except:
            popular_table=popular_table[(popular_table['Description Coverage'] =='')]
            
        popular_table['TotalGrossIncurred']=popular_table['Loss Amount'].shift(-1)
        popular_table=popular_table[popular_table['CarrierClaimNumber'] != ""]        
        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID']
        popular_table.columns=['ClaimStatus', 'LossDate',
        'Action Cause Atty-Suit Loss Location Claimant',
        'Description    Coverage', 'Paid To Date', 'Outstanding Loss Reserves',
        'Expense To Date', 'Recovery To Date', 'Loss Amount',
        'ClaimStatus: ALL Loss Amount = ALL     Deductible Billed',
        'IAB Expense', 'Submission_ID', 'CarrierClaimNumber', 'TotalGrossIncurred']
        found=set(list(popular_table.columns)).intersection(set(cols))
        popular_table=popular_table[found]
        return popular_table
        
    def Firemans_Fund1(self, popular_table):
    
        popular_table=popular_table[~((popular_table['CarrierClaimNumber Status'] =='') & (popular_table['Loss Amount'] == '') )]
        popular_table['CarrierClaimNumber']=popular_table['CarrierClaimNumber Status'].shift(1)
        try:
            popular_table=popular_table[(popular_table['Description    Coverage'] =='')]
        except:
            popular_table=popular_table[(popular_table['Description Coverage'] =='')]
        popular_table['TotalGrossIncurred']=popular_table['Loss Amount'].shift(-1)
        popular_table=popular_table[popular_table['CarrierClaimNumber'] != ""]        
        cols=['CarrierClaimNumber','InsuredName', 'LineOfBusiness', 'PolicyPeriodStart',
            'PolicyPeriodEnd', 'LossDate', 'ClaimStatus',
            'TotalGrossIncurred', 'ValuationDate', 'Submission_ID']
        popular_table.columns=['ClaimStatus', 'LossDate',
        'Action Cause Atty-Suit Loss Location Claimant',
        'Description    Coverage', 'Paid To Date', 'Outstanding Loss Reserves',
        'Expense To Date', 'Recovery To Date', 'Loss Amount',
        'ClaimStatus: ALL Loss Amount = ALL     Deductible Billed',
        'IAB Expense', 'Submission_ID', 'CarrierClaimNumber', 'TotalGrossIncurred']
        found=set(list(popular_table.columns)).intersection(set(cols))
        popular_table=popular_table[found]

        return popular_table