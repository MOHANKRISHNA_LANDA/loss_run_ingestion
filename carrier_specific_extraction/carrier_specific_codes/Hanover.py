import pandas as pd
import numpy as np
import copy 
import sys

class Hanover():

    def __init__(self):
        return 

    def Hanover(self, all_tables):
        """ """
        table_headers = [str(table.columns.tolist()) for table in all_tables ]
        columnnames_df = pd.DataFrame(table_headers, columns=['table_headers'])
        columnnames_df['len'] = columnnames_df['table_headers'].apply(lambda x : len(eval(x)))
        try : 
            len_14_list = columnnames_df[columnnames_df['len'] == 15]['table_headers'].index.tolist()   
            columnnames_df, all_tables = self.table_shrink(len_14_list, columnnames_df, all_tables)    
        except : 
            pass
        try : 
            len_13_list = columnnames_df[columnnames_df['len'] == 14]['table_headers'].index.tolist()
        except : 
            return pd.DataFrame() 

        Master_table = self.looper(len_13_list, all_tables, columnnames_df )
        return Master_table

    def table_shrink(self, index_list, columnnames_df, all_tables) : 
        """ """
        for index in index_list : 
            try : 
                column_no = all_tables[index].columns.tolist().index("")
                all_tables[index] = all_tables[index].drop([all_tables[index].columns[column_no]] , axis=1 )
                columnnames_df['table_headers'][index] = all_tables[index].columns.tolist()
            except ValueError : 
                pass
        return columnnames_df, all_tables

    
    def looper(self, len_13_list, all_tables, columnnames_df):
        """ """
        Master_table = []
        for index in len_13_list : 
            if all_tables[index].shape[0] != 0 : 
                table_1 = self.table_cleaning(all_tables[index])
                table_1['CarrierName'] = "Hanover"
                Master_table.append(table_1)
        return Master_table

    def clean_text(self, text) : 
        text = text.strip()
        text = text.lower()
        return text

    def table_cleaning(self, table_) :
        """ """
        column_names_list= ['CarrierPolicyNumber','CarrierClaimNumber', 'PolicyPeriodStart','PolicyPeriodEnd', 'LossDate', 
                            'NoticeDate','TotalReserve','TotalPaid','ALAE','TotalRecoveries','TotalGrossIncurred','SUBROGATION','ClaimStatus','Submission_ID']
        table_['CoverageType'] = self.clean_text(table_.columns[0]).split("policy #")[0]
        column_names_list.append('CoverageType')
        table_.columns = column_names_list
        table_ = table_.replace('', np.nan , regex=True)
        #finding non-null claim numbers
        claim_no_indexes = []
        original_indexlist = table_.index.tolist()
        for index in original_indexlist: 
            if float(float(table_.loc[index].isna().sum()) / float(table_.shape[1]))  < 0.4 :
                claim_no_indexes.append(index)
    #     print ("claim number indexes : ", claim_no_indexes)
        claimno_ranges = []
        if len(claim_no_indexes) > 1 :
            for i in range(len(claim_no_indexes)) : 
                try :
                    claimno_ranges.append(claim_no_indexes[i+1] - claim_no_indexes[i] - 1 )
                except : 
                    claimno_ranges.append(int(table_.shape[0] - claim_no_indexes[i]))
        else : 
            claimno_ranges = [table_.shape[0] - claim_no_indexes[0]]
            
        keyvalues_list = self.column_creater(claim_no_indexes, claimno_ranges, table_)
        drop_indexes = [x for x in table_.index.tolist() if x not in claim_no_indexes]
        table_1 = table_.drop(index= drop_indexes , axis = 0)
        table_1 = table_1.reset_index()
        table_1 = table_1.drop(columns=['index'] , axis =1 )
        try : 
            table_1['InsuredName'] = pd.Series(keyvalues_list[0])
            table_1['LossDescription_1'] = pd.Series(keyvalues_list[1])
            table_1['LossDescription'] = pd.Series(keyvalues_list[2])
            table_1['Report_run_date'] = keyvalues_list[3]
            table_1['ValuationDate'] = keyvalues_list[4]
        except : 
            pass
        return table_1

    def column_creater(self, claim_no_indexes, claimno_ranges, table_):
        Claimant_names = [] ; Loss_explanations = [] ; Descs = [] ; Report_run_date = '' ; Loss_Evaluation_Date = '' 
        for i in range(len(claim_no_indexes)) : 
            search_rows = []
            for j in range(claimno_ranges[i]) : 
                try : 
                    if claim_no_indexes[i] + j +1 <= table_.shape[0] :
                        search_rows = search_rows + table_.loc[claim_no_indexes[i] + j +1 ].dropna().tolist()
                except ValueError :
                    pass
            search_rows = [self.clean_text(x) for x in search_rows]
            keyvalues_list = self.keyword_search(search_rows)
            Claimant_names.append(keyvalues_list[0])
            Loss_explanations.append(keyvalues_list[1])
            Descs.append(keyvalues_list[2])
            if len(keyvalues_list) == 5 : 
                Report_run_date = keyvalues_list[3]
                Loss_Evaluation_Date = keyvalues_list[4]
        try : 
            return [Claimant_names ,Loss_explanations, Descs , Report_run_date, Loss_Evaluation_Date]
        except :
            return [Claimant_names, Loss_explanations, Descs]

    def keyword_search(self, list_) :
        Claimant_name = "" ; Loss_explanation = "" ; Desc = ""
        for i in range(len(list_)):
            try :
                if "loss explanation:" in list_[i] : 
                    Loss_explanation = list_[i].split("loss explanation:")[1]
                elif "claimant:" in list_[i] : 
                    Claimant_name = list_[i].split("claimant:")[1]
                elif "desc:" in list_[i] : 
                    Desc = list_[i].split("desc:")[1] 
                elif "report run date:" in list_[i] : 
                    Report_run_date = list_[i].split("report run date:")[1]
                elif "loss evaluation date:" in list_[i] :
                    Loss_Evaluation_Date = list_[i].split("loss evaluation date:")[1] 
            except IndexError : 
                pass
        try : 
            return [Claimant_name , Loss_explanation, Desc , Report_run_date , Loss_Evaluation_Date ]
        except : 
            return [Claimant_name , Loss_explanation, Desc ]