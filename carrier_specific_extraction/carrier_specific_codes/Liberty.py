import pandas as pd
import numpy as np


class Liberty():

    def __init__(self):
        print("", end="")
        
    def Liberty(self, df_clean):
        
        df_clean.columns=['CarrierClaimNumber',
        'Claimant Name',
        'LossDate',
        'NoticeDate',
        'Inc BI/MP Paid PD PD O/R', 'Inc PD Paid Expense Expense O/R',
        'Paid Expense',
        'TotalGrossIncurred', '', 'Submission_ID']
    
        df_clean['ClaimStatus']=df_clean['Claimant Name'].shift(-1)
        df_clean['ClosedDate']=df_clean['Claimant Name'].shift(-5)
        df_clean['TotalPaid']=df_clean['Paid Expense'].shift(-1)
        df_clean['LossDescription']=df_clean['CarrierClaimNumber'].shift(-4)
        df_clean['LossState']=df_clean['CarrierClaimNumber'].shift(-5)
        df_clean=df_clean[((df_clean['CarrierClaimNumber']!='') & (df_clean['TotalGrossIncurred'] != '' ))]
        
        return df_clean


    def Liberty1(self, df_clean):
        
        df_clean.columns=['CarrierClaimNumber',
        'Claimant Name',
        'LossDate',
        'NoticeDate',
        'Inc Loss Paid Loss Expense O/R',
        'Inc Expense Paid Expense Outstanding Reserve Applied Recovery',
        'TotalGrossIncurred', '', 'Submission_ID']
        df_clean['ClaimStatus']=df_clean['Claimant Name'].shift(-1)
        df_clean['LineOfBusiness']=df_clean['CarrierClaimNumber'].shift(-1)
        df_clean['ClosedDate']=df_clean['Claimant Name'].shift(-4)
        df_clean['TotalPaid']=df_clean['TotalGrossIncurred'].shift(-1)
        df_clean['LossDescription']=df_clean['CarrierClaimNumber'].shift(-5)
        df_clean['LossState']=df_clean['CarrierClaimNumber'].shift(-4)
        df_clean=df_clean[((df_clean['CarrierClaimNumber']!='') & (df_clean['TotalGrossIncurred'] != '' ) & (df_clean['TotalPaid'] != '' ))]
        
    
        return df_clean


    def Liberty2(self, df_clean):
        
        df_clean.columns=['CarrierClaimNumber',
        'Claimant Name', 'Reopen Date',
        'LossDate',
        'NoticeDate', 'Inc Loss Paid Loss Expense O/R',
        'Inc Expense Paid Expense Outstanding Reserve Applied Recovery',
        'TotalGrossIncurred', 'Submission_ID']
        
        df_clean['ClaimStatus']=df_clean['Claimant Name'].shift(-1)
        df_clean['LineOfBusiness']=df_clean['CarrierClaimNumber'].shift(-1)
        df_clean['ClosedDate']=df_clean['Claimant Name'].shift(-4)
        df_clean['TotalPaid']=df_clean['TotalGrossIncurred'].shift(-1)
        df_clean['LossDescription']=df_clean['Claimant Name'].shift(-5)
        df_clean['LossState']=df_clean['CarrierClaimNumber'].shift(-4)
        df_clean=df_clean[((df_clean['CarrierClaimNumber']!='') & (df_clean['TotalGrossIncurred'] != '' ) & (df_clean['TotalPaid'] != '' ))]
        
        
        return df_clean