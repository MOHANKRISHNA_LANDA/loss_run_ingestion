import pandas as pd 
import numpy as np
class Cincinnati() : 

    def __init__(self) :
        self.columns = [ 'Sorted by Date of Loss  Loc', 'CarrierPolicyNumber', 'PolicyPeriodStart',
                        'InsuredName', 'Oc #', 'Cat', 'LossDate', 'LossDescription',
                        'CoverageType', 'Claimant/Payee', 'TotalPaid', 'Salv/Subr', 'Expense',
                        'End Rsv or Month Closed', 'TotalGrossIncurred',
                        'Submission_ID']
    
    def Cincinnati(self, popular_table):
        """
        """
        popular_table.columns =self.columns
        popular_table=popular_table[(popular_table['LossDescription']!='') & (popular_table['CoverageType']!='') ]
        popular_table=popular_table.replace(r'^\s*$', np.nan, regex=True)
        popular_table.fillna(method='ffill',inplace=True)

        return popular_table