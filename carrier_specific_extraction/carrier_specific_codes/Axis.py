import pandas as pd 
import numpy as np 


class Axis():

    def __init__(self):
        print("", end="")

    def Axis(self, popular_table):

        popular_table.columns=['CarrierClaimNumber', 'LossState',
        'LossDate', 'NoticeDate',
        'ClaimStatus', 'ClosedDate', 'Coverage:', 'CoverageType', 'Loss Description', '',
        'Submission_ID']
        popular_table=popular_table[(popular_table['NoticeDate'] != "")]
        popular_table=popular_table[(popular_table['LossDate'] != "")]
        popular_table['TotalPaid'] = popular_table['CarrierClaimNumber'].shift(-1)
        popular_table['TotalGrossIncurred'] = popular_table['NoticeDate'].shift(-1)
        popular_table=popular_table[(popular_table['ClaimStatus'] != "")]
        return popular_table

    def Axis1(self, popular_table):

        popular_table.columns=['CarrierClaimNumber', 'LossState',
        'LossDate', 'NoticeDate',
        'ClaimStatus', 'ClosedDate', 'Coverage:', 'CoverageType', 'Loss Description', '',
        'Submission_ID']
        popular_table=popular_table[(popular_table['NoticeDate'] != "")]
        popular_table=popular_table[(popular_table['LossState'] != "")]
        popular_table['TotalPaid'] = popular_table['CarrierClaimNumber'].shift(-1)
        popular_table['TotalGrossIncurred'] = popular_table['NoticeDate'].shift(-1)
        popular_table=popular_table[(popular_table['ClaimStatus'] != "")]
        return popular_table

    def Axis2(self, popular_table):

        popular_table.columns=['CarrierClaimNumber', 'LossState',
        'LossDate', 'NoticeDate',
        'ClaimStatus', 'ClosedDate', 'Coverage:', 'CoverageType', 'Loss Description', '',
        'Submission_ID']
        popular_table=popular_table[(popular_table['NoticeDate'] != "")]
        popular_table=popular_table[(popular_table['LossDate'] != "")]
        popular_table=popular_table[(popular_table['CarrierClaimNumber'] != "")]
        popular_table['TotalPaid'] = popular_table['CarrierClaimNumber'].shift(-1)
        popular_table['TotalGrossIncurred'] = popular_table['NoticeDate'].shift(-1)
        popular_table=popular_table[(popular_table['ClaimStatus'] != "")]
        return popular_table