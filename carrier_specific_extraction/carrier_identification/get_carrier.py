import pdf2image 
import pandas as pd 
from PyPDF2 import PdfFileReader
import cv2
import glob
import os 
import re 
from tqdm import tqdm
from collections import defaultdict
from pdf2image import convert_from_path


class Carrier_Identification() : 
    
    def __init__(self) : 
        
        self.output_dir = 'carrier_identification/images/'
        self.template_dir = 'carrier_identification/templates/'
        self.carrier_keywords = {'Hanover': ['Hanover Insurance'], 'Hartford': ['Hartford', 'hartford']}

    def preprocess_text(self, string_) : 
        string_ = re.sub(r'[^\x00-\x7F]+',' ', string_)
        string_ = re.sub(r'(\n|\t|\r)' , ' ' , string_)
        string_ = re.sub(r'[\x00-\x08\x0b\x0c\x0e-\x1f\x7f-\xff]', ' ', string_)
        string_ = re.sub(r'[^\w\ ]+', ' ' , string_)
        string_ = re.sub(r' +' , ' ' , string_)
        return string_

    def Merge(self, img_dict, text_dict): 
        merged_dict = {}
        for key in img_dict.keys() : 
            if key != "other" :
                merged_dict[key] = list(set(img_dict[key]).union(set(text_dict[key])))
            else : 
                merged_dict[key] = list(set(img_dict[key]))
        return merged_dict

    def get_pdf_images(self, filenames):
        
        for i in range(len(filenames)):
            filename = str(filenames[i].split("/")[-1].split(".pdf")[0])
            try:
                images = convert_from_path(filenames[i],dpi=200,first_page=1,last_page=1)
                for index, val in enumerate(images):
                    val.save(self.output_dir+'/'+filename+'_'+str(index)+".jpg", "JPEG")
            except:
                continue
        return glob.glob(self.output_dir+"/*.jpg")

    def template_images_read(self) : 

        template_names = glob.glob(f'{self.template_dir}/*.jpg')
        template_images = []
        for template in template_names:
            template_images.append(cv2.imread(template,0))
        return template_images, list(map(lambda x : x.split("/")[-1].split("_")[0] , template_names))


    def text_matching(self, filenames) :
        
        text_dict = {}
        for filename in filenames : 
            try : 
                f = os.popen(f'pdftotext -layout {filename} -')
                text = self.preprocess_text(f.read())
                filename = filename.split('/')[-1].split('.txt')[0]
                text_dict[filename] = text
            except Exception as e :
                print(f'Reading error {filename}, {e}' )
                pass 
        carrier_file_list = defaultdict(list)
        for fkey in text_dict:
            for key in self.carrier_keywords.keys() : 
                if any(keyword in text_dict[fkey] for keyword in self.carrier_keywords[key]):
                    carrier_file_list[key].append(fkey.strip(".pdf"))
        return carrier_file_list

    def image_matching(self, all_first ):

        threshold_dict = {'Hartford': 0.59 , 'Hanover': 0.63}
        template_images, template_names = self.template_images_read()
        carrier_file_list = defaultdict(list)
        for img_name in all_first:
            match_percentage = []
            try:
                img_gray = cv2.imread(img_name,0)
            except Exception as e:
                continue
            try:
                for template_img, template_name in zip(template_images, template_names) :
                    res_ = cv2.matchTemplate(img_gray, template_img, cv2.TM_CCOEFF_NORMED)
                    match_percentage.append((template_name, res_.max()))
            except Exception as e:
                print(e)
                pass
            
            if match_percentage:
                passed_over = False
                for match in match_percentage : 
                    if match[1] > threshold_dict[match[0]] : 
                        carrier_file_list[match[0]].append(img_name.split("/")[-1].split(".")[0].split("_")[0])
                        passed_over = True
                if passed_over is False :
                    carrier_file_list["other"].append(img_name.split("/")[-1].split(".")[0].split("_")[0])
        return carrier_file_list

    def CI_Main(self, filenames) : 
        
        all_first = self.get_pdf_images(filenames)
        img_dict  = self.image_matching(all_first)
        os.system("rm " + self.output_dir + "/*") 
        # print("Image Classification Done")
        txt_dict  = self.text_matching(filenames)
        # print("Text Classification Done")
        distribution_dict = self.Merge(img_dict, txt_dict)
        return distribution_dict