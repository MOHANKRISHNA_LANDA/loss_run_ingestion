import pandas as pd
import camelot 
import re


#usage
#filename = 'P318101613254938.pdf'
#df_cam = camelot.read_pdf(filename, pages = '1,2' , flavor="stream" , split_text = True)

#tabs=slice_multiple_tables(hr.clean_table(df_cam[2].df))


def slice_table(tt):
    """Creates a new table with header"""
    try:
        ct=tt.copy()
        ct.index=range(len(ct))
        head_no=list(ct[ct['header_no']!='0']['header_no'])
        header=ct[ct['header_no']==head_no[0]].values[0]
        head_index=list(ct[ct['header_no']==head_no[0]].index)[0]

        del ct['header_no']
        del ct['concat_column']

        cols=header[:-2]
        #print('Header',cols)
        ct.columns=cols
        ct=ct[head_index+1:len(ct)]
    except:
        #print('exception')
        return ct
    return ct

def slice_multiple_tables(ct_tab):
    """Extracts multiple tables from stream output
    #usage
    #filename = 'P318101613254938.pdf'
    #df_cam = camelot.read_pdf(filename, pages = '1,2' , flavor="stream" , split_text = True)
    #tabs=slice_multiple_tables(hr.clean_table(df_cam[2].df))
    
    """
    ct_tab.index=range(len(ct_tab))

    headers_index=list(ct_tab[ct_tab['header_no']!='0'].index)
    #print('tables found ',len(headers_index))
    tables=[]
    for i in range(len(headers_index)):

        if i<len(headers_index)-1:
            #print(i)
            st_index=headers_index[i]
            end_index=headers_index[i+1]-1
            table=ct_tab.iloc[st_index:end_index]
            tables.append(slice_table(table.copy()))
        else:
            st_index=headers_index[i]
            end_index=len(ct_tab)
            table=ct_tab.iloc[st_index:end_index]
            tables.append(slice_table(table.copy()))
    return tables   
	

def clean_table(table):
    tab_normalized,pad_rows= normalize_table(table)
    clean_tab=merge_multiline_headers(tab_normalized,pad_rows)
    return clean_tab


def normalize_table(table):
    rows=[]
    for row in range(len(table)):
        rows.append(list(table.loc[row]))


    new_rows=[]
    for row in rows:
        new_row=[]
        for col in row:
            new_row=new_row+col.split('\n')
        new_rows.append(new_row)


    lengths=[]
    for i in new_rows:
        #print(len(i))
        lengths.append(len(i))

    max_len=max(lengths)

    pad_rows=[]
    for row in new_rows:
        current_length=len(row)
        pad_length=max_len-current_length
        #print(pad_length)
        pad_row=row+['']*pad_length
        pad_rows.append(pad_row)

    norm_df=pd.DataFrame(pad_rows)
    
    return norm_df,pad_rows


def merge_multiline_headers(tab_normalized,pad_rows):
    #is num found in a row or not
    line_num=[]
    for i in range(len(tab_normalized)):
        line=''.join(list(tab_normalized.loc[i]))
        if len(re.findall('\d+',line))>0:

            line_num.append(1)
        else:
            line_num.append(0)

    #consecutive non-number sequences grouping
    seqs=[]
    i=0
    while(i<len(line_num)):
        next_i=0
        if i <len(line_num)-1:
            if line_num[i]==0:
                seq=[i]
                for j in range(i+1,len(line_num)):
                    if line_num[j]==1:
                        break
                    if line_num[j]==0:
                        seq.append(j)
                #print(seq)  
                if len(seq)>1:
                    seqs.append(seq)
                next_i=max(seq)+1
        if next_i!=0:
            i=next_i
        else:
            i=i+1



    #concatenating consecutive headers
    headers=[]
    for seq in seqs:
        new_row=['']*len(pad_rows[0])
        for row in seq:
            current_row=pad_rows[row]
            for col_no in range(len(current_row)):
                new_row[col_no]=new_row[col_no]+' '+current_row[col_no]
        headers.append(new_row)


    #filtering headers of length <5
    fil_headers_seqs=[]
    fil_headers=[]
    for head in range(len(headers)):
        if len(set((headers[head])))>5:
            fil_headers_seqs.append(seqs[head])
            fil_headers.append(headers[head])
        set(headers[0])

    tab_normalized['header_no']=0
    #overwriting the table with concatenated headers
    header_no=1
    for head in range(len(fil_headers_seqs)):
        for line_no in fil_headers_seqs[head]:
            tab_normalized.loc[line_no]=fil_headers[head]+[header_no]
        header_no=header_no+1


    tab_normalized['header_no']=tab_normalized['header_no'].astype(str)

    #unique key
    #unique key
    tab_normalized['concat_column']=tab_normalized[0]
    for i in range(1, tab_normalized.columns.tolist().index("header_no")+1) : 
        tab_normalized['concat_column'] += tab_normalized[tab_normalized.columns[i]]
    
    final_table=tab_normalized.drop_duplicates('concat_column')
    
    return final_table
